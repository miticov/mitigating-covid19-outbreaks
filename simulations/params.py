import matplotlib as mpl
import matplotlib.pyplot as plt
from matplotlib.backends.backend_pdf import PdfPages
import pandas as pd
import numpy as np
import functools
import random
from scipy.stats import gamma

from utils import *

debug = False

################################################################################

data = dict()
def load_csv(filename):
  global data
  
  print("load", filename)
  if filename not in data: 
    table = pd.read_csv(filename)
    
    table["nbSymp"] = table.filter(regex="nbSympt*").sum(axis=1)
    table["nbAsympt"] = table.filter(regex="nbAsympt*").sum(axis=1)
    table["nbInfected"] = table["nbSymp"] + table["nbAsympt"]
    for label in set(table.startLabel):
      table["nbInfected" + str(label)] = table["nbSympt" + str(label)] \
                                       + table["nbAsympt" + str(label)]
    
    description = filename
    return (table, description) # memory usage
    data[filename] = (table, description)
  
  return data[filename]

target = 1.25

param = dict(param_default)
for param["graph"] in param_all["graph"]:
  X, Y = [], []
  for param["p"] in param_all["p"][param["graph"]]:
    X.append(float(param["p"]))
    Y.append(load_csv(getFilenameData(param))[0]["nbInfectedGen1"].mean())
  X, Y = zip(*sorted(zip(X, Y)))
  plt.plot(X, Y, "o-")
  plt.show()
  for i in range(1, len(X)):
    if Y[i-1] < target < Y[i]:
      print(Y[i-1], Y[i])
      print(X[i-1], X[i])
      print(X[i] - (X[i] - X[i-1]) * (Y[i] - target) / (Y[i] - Y[i-1]))
