import matplotlib as mpl
import matplotlib.pyplot as plt
from matplotlib.backends.backend_pdf import PdfPages
import pandas as pd
import numpy as np
import functools
import random, math
from scipy.stats import gamma, nbinom, linregress

from utils import *

debug = False
sizeplot = 5

################################################################################

strat_color = {
  "none": plt.cm.tab10(5),
  "onoff1d": plt.cm.tab10(3),
  "onoff1w": plt.cm.tab10(1),
  "rotation1d": plt.cm.tab10(4),
  "rotation1w": plt.cm.tab10(0),
  "closed": plt.cm.tab10(2),
  "work": plt.cm.tab10(6),
  "onoff-4-10": plt.cm.tab10(7),
  "rotation-4-10": plt.cm.tab10(8),
}

################################################################################

data = dict()
def load_csv(filename):
  global data
  
  print("load", filename)
  if filename not in data: 
    table = pd.read_csv(filename)
    
    table["nbSymp"] = table.filter(regex="nbSympt*").sum(axis=1)
    table["nbAsympt"] = table.filter(regex="nbAsympt*").sum(axis=1)
    table["nbInfected"] = table["nbSymp"] + table["nbAsympt"]
    for label in set(table.startLabel):
      table["nbInfected" + str(label)] = table["nbSympt" + str(label)] \
                                       + table["nbAsympt" + str(label)]
    
    description = filename
    return (table, description) # memory usage
    data[filename] = (table, description)
  
  return data[filename]

def generationInterval(t, sympt=None):
  cond = t["nbInfected"] > 0
  if sympt != None:
    cond = cond & (t["startSympt"] == sympt)
  avg = (t[cond]["lengthGen1"] * t[cond]["nbInfectedGen1"]).sum()
  if avg == 0: return 0
  avg /= t[cond]["nbInfectedGen1"].sum()
  return avg

################################################################################

def incertitude(c):
  if len(c) == 0: return 0
  return 5 * c.std() / math.sqrt(len(c))

def kappa(c):
  # assumes that c is sampled from NegBinom(K, P = K/(K+E))
  E = c.mean() # E = K * (1-P) / P
  V = c.var()  # V = K * (1-P) / P^2
  K = E ** 2 / (V - E)
  return K

################################################################################

def genBins(mini, maxi):
  return np.arange(mini-0.5, maxi+1, 1)

def plotHist(ax1, col, title="", bins=None, swapXY=False):
  if bins is None: bins = genBins(col.min(), col.max())
  if swapXY:
    hline = ax1.axvline
    ax2 = ax1.twiny()
    vline = ax2.axhline
    orientation="horizontal"
    ax1.set_xlabel("Probability")
    ax1.set_ylabel(title)
    ax1.set_ylim((bins[0],bins[-1]))
  else:
    hline = ax1.axhline
    ax2 = ax1.twinx()
    vline = ax2.axvline
    orientation="vertical"
    ax1.set_xlabel(title)
    ax1.set_ylabel("Probability")
    ax1.set_xlim((bins[0],bins[-1]))
  
  hline(0.05, color='gray', linestyle='dashed', linewidth=1)
  hline(0.95, color='gray', linestyle='dashed', linewidth=1)
  ax1.hist(col, bins=bins, density=True,
    histtype='step',cumulative=1, orientation=orientation,
   color="darkorange", linewidth=1)
 
  ax2.set_axis_off()
  ax2.hist(col, bins=bins, alpha=.3, orientation=orientation)
  vline(col.quantile(.05), color='gray', linestyle='dashed', linewidth=1)
  vline(col.quantile(.95), color='gray', linestyle='dashed', linewidth=1)


#vmax=100 * len(data) / (len(bins1) * len(bins2))
def plotData2(fig, col1, col2, bins1=None, bins2=None, vmax=100):
  if bins1 is None: bins1 = genBins(col1.min(), col1.max())
  if bins2 is None: bins2 = genBins(col2.min(), col2.max())
  
  plotHist(fig.add_subplot(2,2,1), col1, col1.name, bins1)
  plotHist(fig.add_subplot(2,2,4), col2, col2.name, bins2, swapXY=True)
  ax = fig.add_subplot(2,2,3)
  norm=mpl.colors.LogNorm(vmin=1, vmax=vmax)
  cmap = plt.cm.viridis
  cmap.set_bad('k')
  #ax.hist2d(col1, col2, bins=(bins1,bins2), norm=norm, cmap=cmap)
  im,_,_ = np.histogram2d(col1, col2, bins=(bins1,bins2))
  ax.imshow(im.T, norm=norm, cmap=cmap, aspect="auto", origin="lower",
    extent=(bins1[0],bins1[-1],bins2[0],bins2[-1]))
  ax.set_xlabel(col1.name)
  ax.set_ylabel(col2.name)

def getText(col, vals):
  s = "E[%s] = %f" % (col.name, col.mean())
  for v in vals:
    s += "\nP[%s > %d] = %f" % (col.name, v, sum(col > v) / len(col))
  return s

################################################################################

def figure_nbInfected_lengthEpidemy(table, description):
  global sizeplot
  fig = plt.figure(figsize=(2*sizeplot,2*sizeplot))
  plotData2(fig, table["nbInfected"], table["lengthEpidemy"])
  text = description
  text += "\n" + getText(table["nbInfected"], [0, 5, 10]) + "\n"
  text += "\n" + getText(table["lengthEpidemy"], [10, 50, 100]) + "\n"
  fig.text(0.6, 0.85, text, va="top")
  fig.tight_layout()
  return fig
  
def figure_nbInfectedLabel(table, description):
  global sizeplot
  
  labels = sorted(list(set(table.startLabel)))
  grid = 1 + int(np.sqrt(len(labels)+1))
  fig = plt.figure(figsize=(sizeplot*grid,sizeplot*grid))
  
  fig.text(0.1+0.2/grid, 0.9-0.2/grid, description, va="top")
  
  for i,label in enumerate(labels):
    ax = fig.add_subplot(grid,grid,i+2)
    col = "nbInfected" + label
    bins = genBins(table[col].min(), table[col].max())
    
    ax.hist(table[col],
      bins=bins, density=True, histtype='step',cumulative=1,
      linewidth=1, label="no conditionning")
    
    ax.hist(table[col][table.startLabel==label],
      bins=bins, density=True, histtype='step',cumulative=1,
      linewidth=1, label="startLabel inside")
    
    ax.hist(table[col][table.startLabel!=label],
      bins=bins, density=True, histtype='step',cumulative=1,
      linewidth=1, label="startLabel outside")
    
    ax.set_ylabel("Probability")
    ax.set_xlabel(col)
    ax.set_xlim((bins[0],bins[-1]))
    ax.legend()
  
  fig.tight_layout()
  return fig

def figure_generation(table, description):
  global sizeplot
  
  fig = plt.figure(figsize=(2*sizeplot,2*sizeplot))
  #plotHist(fig.add_subplot(2,2,1), table["nbInfected"], "nbInfected")
  col1, col2 = table["nbInfectedGen1"], table["nbInfectedGen2"]
  plotHist(fig.add_subplot(2,2,3), col1, col1.name)
  plotHist(fig.add_subplot(2,2,4), col2, col2.name)
  
  ax = fig.add_subplot(2,2,1)
  bins = genBins(col1.min(), col1.max())
  ax.hist(col1, bins=bins, alpha=.3, density=True)
  R, K = col1.mean(), kappa(col1)
  rv = nbinom(K, K / (R + K))
  vals = range(col1.min(), col1.max()+1)
  ax.plot(vals, [rv.pmf(i) for i in vals], "x", color="blue")
  ax.set_yscale("log")
  ax.set_xlabel("nbInfectedGen1")
  ax.set_ylabel("Probability")
  
  text = description
  text += "\n" + getText(table["nbInfected"], [0, 5, 10]) + "\n"
  text += "\n" + getText(col1, [0, 1, 2]) + "\n"
  text += "\n" + getText(col2, [0, 1, 2]) + "\n"
  fig.text(0.6, 0.85, text, va="top")
  fig.tight_layout()
  return fig

def figure_lengthInfected(table, description):
  global sizeplot

  plots = [
    (1,"lengthEpidemy"),
    (2,"length5Infected"),
    (3,"length10Infected"),
  ]
  fig = plt.figure(figsize=(sizeplot*len(plots),sizeplot))
  for i,c in plots:
    plotHist(fig.add_subplot(1,3,i), table[c], c)
  text = description
  fig.text(0.1, 0.4, text, va="top")
  fig.tight_layout()
  return fig

def figure_days(target, param, filenames):
  global param_description, sizeplot
  
  tables = dict()
  for strat in filenames:
    tables[strat] = load_csv(filenames[strat][None])[0]
  
  with PdfPages(target) as pdf:
    fig = plt.figure(figsize=(3*sizeplot,2*sizeplot))
    strats = ["none", "onoff1d", "rotation1d", "closed", "onoff1w", "rotation1w"]
    for i,strat in enumerate(strats):
      ax = fig.add_subplot(2,3,1+i)
      A = [ tables[strat]["incidenceDay%d"%i].mean() for i in range(14)]
      Ae = [ incertitude(tables[strat]["incidenceDay%d"%i]) for i in range(14)]
      B = tables[strat]["nbInfected"].mean()
      Be = incertitude(tables[strat]["nbInfected"])
      vals = np.array([ 14 * a/B for a in A])
      errs = np.array([ 14 * (a/B) * (ae/a + Be/B) for a,ae in zip(A,Ae)])
      ax.bar(range(14), vals, yerr=errs, capsize=3, color=strat_color[strat])  
      #ax.fill_between(range(14), vals-errs, vals+errs, color="blue", alpha=0.1)
      ax.set_title(strat_description[strat])
      ax.set_xlabel("day")
      ax.set_ylabel("14 $\cdot$ E[nbInfectedDay] / E[nbInfected]")
      ax.set_ylim(0, 2.25)
    fig.tight_layout()
    pdf.savefig()
    plt.close()
    
    fig,ax = plt.subplots(figsize=(8,8))
    for strat in filenames:
      vals = [ tables[strat]["incidenceDay%d"%i].mean()
             / tables[strat]["prevalence"].mean() for i in range(14)]
      ax.plot(vals, "o-", label=strat_description[strat], color=strat_color[strat])
    ax.set_xlabel("day")
    ax.set_ylabel("E[incidence] / E[total prevalence]")
    ax.set_ylim(bottom=0)
    ax.legend()
    fig.tight_layout()
    pdf.savefig()
    plt.close()

def figure_coupling(target, param, filenames):
  global param_description, sizeplot
  
  strats = list(filenames)
  
  tables = dict()
  for strat in strats:
    tables[strat] = load_csv(filenames[strat][None])[0]
  
  t, r = dict(), dict()
  for strat in strats:
    t[strat] = tables[strat]["nbInfected"].to_numpy(copy=True)
    r[strat] = tables[strat]["nbInfected"].to_numpy(copy=True)
    random.shuffle(r[strat])
  
  N = len(tables["none"])
  domination, randomized = dict(), dict()
  for s1 in strats:
    domination[s1], randomized[s1] = dict(), dict()
    for s2 in strats:
      st, sr = sum(t[s1] > t[s2]), sum(r[s1] > r[s2])
      domination[s1][s2] = (st, N, 100*st/N)
      randomized[s1][s2] = (sr, N, 100*sr/N)
  
  with PdfPages(target) as pdf:
    
    fig, ax = plt.subplots(figsize=(10,3))
    cells = [[ "%d/%d = %.1f%%" % domination[s1][s2]
                for s2 in strats] for s1 in strats]
    ax.table(cellText=cells, colLabels=strats, rowLabels=strats,loc="center")
    ax.get_xaxis().set_visible(False)
    ax.get_yaxis().set_visible(False)
    ax.set_frame_on(False)
    ax.set_title("With coupling")
    #fig.tight_layout()
    pdf.savefig()
    plt.close()
    
    fig, ax = plt.subplots(figsize=(10,3))
    cells = [[ "%d/%d = %.1f%%" % randomized[s1][s2]
                for s2 in strats] for s1 in strats]
    ax.table(cellText=cells, colLabels=strats, rowLabels=strats,loc="center")
    ax.get_xaxis().set_visible(False)
    ax.get_yaxis().set_visible(False)
    ax.set_frame_on(False)
    ax.set_title("Without coupling")
    #fig.tight_layout()
    pdf.savefig()
    plt.close()

def figure_summary(target, param, key, filenames):
  global debug, param_description, sizeplot
  
  default = getParamDefault(param, key)
  compare = lambda v: (v[0] != default, float(v[0]))
  # load data
  values, tables = dict(), dict()
  for strat in filenames:
    values[strat], tables[strat] = [], []
    for v in filenames[strat]:
      values[strat].append(v)
      tables[strat].append(load_csv(filenames[strat][v][None])[0])
    values[strat],tables[strat] = zip(*sorted(
      zip(values[strat],tables[strat]),
      key=compare))
    
  plots = [
      ("P[Outbreak]", "steelblue", (0, 0.5),
        lambda t: t["nbInfected"] >= 5),
      ("E[Nb days until Outbreak | Outbreak]", "green", (0, 20),
        lambda t: t[t["nbInfected"] >= 5]["length5Infected"]),
      ("E[Nb infected | Outbreak]", "brown", (0,120),
        lambda t: t[t["nbInfected"] >= 5]["nbInfected"])]
  
  with PdfPages(target) as pdf:
    for v in range(len(values["none"])):
    
      fig = plt.figure(figsize=(1.5*sizeplot,1.5*sizeplot))
      R0 = tables["none"][v]["nbInfectedGen1"].mean()
      R0e = incertitude(tables["none"][v]["nbInfectedGen1"])
      fig.text(0.75, 0.98, "$R_0 = %.2f \pm %.2f$" % (R0, R0e))
      
      for i, (desc, color, xlim, fun) in enumerate(plots):
        ax = fig.add_subplot(len(plots), 1, i+1)
        ax.patch.set_facecolor(color)
        ax.patch.set_alpha(0.15)
        strats = list(filenames)
        ys = list(reversed(range(len(strats))))
        labels = [ strat_description[strat] for strat in strats ]
        colors = [ strat_color[strat] for strat in strats ]
        vals, errs = [], []
        for strat in strats:
          c = fun(tables[strat][v])
          vals.append(c.mean())
          errs.append(incertitude(c))
        ax.barh(ys, vals, xerr=errs, tick_label=labels,
          height=.4, capsize=3, color=colors)
        ax.xaxis.grid(True, linestyle='--', which='major',
                      color='grey', alpha=.25)
        ax.set_xlim(xlim)
        ax.set_title(desc)
      
      fig.tight_layout()
      pdf.savefig()
      plt.close() 

def figure_data(target, param, key, filenames):
  global debug, param_description, sizeplot
  
  default = getParamDefault(param, key)
  compare = lambda v: (v[0] != default, float(v[0]))
  # load data
  values, tables = dict(), dict()
  for strat in filenames:
    values[strat], tables[strat] = [], []
    for v in filenames[strat]:
      values[strat].append(v)
      tables[strat].append(load_csv(filenames[strat][v][None])[0])
    values[strat],tables[strat] = zip(*sorted(
      zip(values[strat],tables[strat]),
      key=compare))
  
  with PdfPages(target) as pdf:
    for v in range(len(values["none"])):
      fig = plt.figure(figsize=(3*sizeplot,sizeplot))
      cell_text, cell_color = [], []
      headers = [
        "Strategy",
        "R",
        "P[Outbreak]", 
        "E[Nb days until Outbreak | Outbreak]", 
        "E[Nb infected | Outbreak]"]
      for strat in filenames:
        line = [strat_description[strat]]
        t = tables[strat][v]
        c = t["nbInfected"] >= 5
        line.append("$%.2f \pm %.2f$" % \
          (t["nbInfectedGen1"].mean(),
           incertitude(t["nbInfectedGen1"])))
        line.append("$%.1f \pm %.1f$" % \
          (100*c.mean(), 100*incertitude(c)))
        line.append("$%.1f \pm %.1f$" % \
          (t["length5Infected"][c].mean(),
           incertitude(t["length5Infected"][c])))
        line.append("$%.1f \pm %.1f$" % \
          (t["nbInfected"][c].mean(),
            incertitude(t["nbInfected"][c])))
        cell_text.append(line)
        color1 = mpl.colors.to_rgba("steelblue")[:-1] + (0.15,)
        color2 = mpl.colors.to_rgba("green")[:-1] + (0.15,)
        color3 = mpl.colors.to_rgba("brown")[:-1] + (0.15,)
        cell_color.append(["white", "white", color1, color2, color3])
      plt.title("%s = %s" % (param_description[key], str(float(values["none"][v]))))
      table = plt.table(cellText=cell_text, cellColours=cell_color,
                        colLabels=headers, loc="center")
      table.auto_set_font_size(False)
      table.set_fontsize(10)
      table.scale(1, 2)
      plt.axis("off")
      fig.tight_layout()
      pdf.savefig()
      plt.close() 


def figure_sensitivity_R(target, param, keys, filenames):
  global debug, params_description, sizeplot
  
  # load data
  values, tables = dict(), dict()
  for strat in filenames:
    values[strat], tables[strat] = [], []
    for v in filenames[strat]:
      values[strat].append(v)
      tables[strat].append(load_csv(filenames[strat][v][None])[0])
    values[strat],tables[strat] = zip(*sorted(
      zip(values[strat],tables[strat]),
      key=lambda v: (float(v[0]),v[0])))
  
  plots = [
      ("$R_e$", "white", (0, 2.5), (0, 2.5),
        lambda t: t["nbInfectedGen1"]),
      ("P[Outbreak]", "steelblue", (0, 2.5), (0,.5),
        lambda t: t["nbInfected"] >= 5),
      ("E[Nb days until outbreak | Outbreak]", "green", (0, 2.5), (0, 20),
        lambda t: t[t["nbInfected"] >= 5]["length5Infected"]),
      ("E[Nb infected | Outbreak]", "brown", (0, 2.5), (0, 200),
        lambda t: t[t["nbInfected"] >= 5]["nbInfected"])]
  
  with PdfPages(target) as pdf:
    for desc, color, xlim, ylim, fun in plots:
      fig,ax = plt.subplots(figsize=(sizeplot,sizeplot))
      ax.patch.set_facecolor(color)
      ax.patch.set_alpha(0.15)
      R0 = [t["nbInfectedGen1"].mean() for t in tables["none"]]
      R0e = [incertitude(t["nbInfectedGen1"]) for t in tables["none"]]
      default = R0[values["none"].index(getParamDefault(param, "p"))]
      ax.axvline(default, linestyle=":", color="k")
      if desc == "$R_e$":
        ax.axhline(1, color="grey", alpha=.5)
      
      for strat in filenames:
        vals = [fun(t).mean() for t in tables[strat]]
        errs = [incertitude(fun(t)) for t in tables[strat]]
        """
        if desc == "$R_e$" and strat in ["none", "onoff1w", "rotation1w", "closed"]:
          slope, intercept, _, _, _ = linregress(R0, vals)
          angle = math.atan(slope) * 180 / math.pi
          ax.plot([0,2.5], [intercept, intercept+slope*2.5], "k", linewidth=.2)
          text = "$R_e = %.2f \cdot R_0"+("+" if intercept>=0 else "")+"%.2f$"
          text = text % (slope, intercept)
          ax.text(2.25, intercept + 2.1 * slope, text,
            fontsize=5, ha="center", va="center", rotation=angle)
          #ax.plot([0,R0[2]], [0, vals[2]], "k:")
        """
        ax.errorbar(R0, vals, xerr=R0e, yerr=errs,
          label=strat_description[strat], color=strat_color[strat])
        lower = [vals[i]-errs[i] for i in range(len(vals))]
        upper = [vals[i]+errs[i] for i in range(len(vals))]
        for i in range(1, len(R0)):
          dl = (vals[i] - vals[i-1]) / (R0[i] - R0e[i] - R0[i-1] + R0e[i-1])
          dr = (vals[i] - vals[i-1]) / (R0[i] + R0e[i] - R0[i-1] - R0e[i-1])
          lower[i-1] = min(lower[i-1], vals[i-1]+R0e[i-1]*dl, vals[i-1]-R0e[i-1]*dr)
          upper[i-1] = max(upper[i-1], vals[i-1]+R0e[i-1]*dl, vals[i-1]-R0e[i-1]*dr)
          lower[i] = min(lower[i], vals[i]-R0e[i]*dr, vals[i]+R0e[i]*dl)
          upper[i] = max(upper[i], vals[i]-R0e[i]*dr, vals[i]+R0e[i]*dl)
        ax.fill_between(R0, lower, upper, color=strat_color[strat], alpha=0.1)
      ax.grid(True, which='major', linestyle="--", color='grey', alpha=.25)
      ax.set_xlabel("$R_0$")
      ax.set_ylabel(desc)
      ax.set_xlim(xlim)
      ax.set_ylim(ylim)
      ax.legend()
      fig.tight_layout()
      pdf.savefig()
      plt.close() 

def figure_sensitivity(target, param, key, filenames):
  global debug, param_description, sizeplot
  
  # load data
  values, tables = dict(), dict()
  for strat in filenames:
    values[strat], tables[strat] = [], []
    for v in filenames[strat]:
      values[strat].append(v)
      tables[strat].append(load_csv(filenames[strat][v][None])[0])
    values[strat],tables[strat] = zip(*sorted(
      zip(values[strat],tables[strat]),
      key=lambda v: (float(v[0]),v[0])))
  
  # multipage pdf
  with PdfPages(target) as pdf:
  
    # Page 1: distributions
    if key in ["p","e","s"]:
      fig, ax = plt.subplots(figsize=(1.5*sizeplot,sizeplot))
      ps = list(map(float, values["none"]))
      pks = [float(getParam(param, key+"k"))] * len(ps)
      graph_gamma(ax, ps, pks, k in ["e","s"])
      ax.set_title("distributions")
      fig.tight_layout()
      pdf.savefig()
      plt.close()
    if k in ["pk","ek","sk"]:
      fig, ax = plt.subplots(figsize=(1.5*sizeplot,sizeplot))
      pks = list(map(float, values["none"]))
      ps = [float(getParam(param, k[:-1]))] * len(pks)
      graph_gamma(ax, ps, pks, key in ["ek","sk"])
      ax.set_title("distributions")
      fig.tight_layout()
      pdf.savefig()
      plt.close()
    
    plots = [
      [
        ("P[Outbreak]", "steelblue",
          lambda t: t["nbInfected"] >= 5)
      ],
      [
        ("E[Nb days until outbreak | Outbreak]", "green",
          lambda t: t[t["nbInfected"] >= 5]["length5Infected"])
      ],
      [
        ("E[Nb infected | Outbreak]", "brown",
          lambda t: t[t["nbInfected"] >= 5]["nbInfected"])
      ],
      [ # Page: R, proba explosion, attack rate
        ("R", "white",
          lambda t: t["nbInfectedGen1"]),
        ("K", "white",
          lambda t: np.array([kappa(t["nbInfectedGen1"])]))
      ],
      [ # Page: proba explosion, attack rate
        ("$\mathrm{P}[nbInfected \geq 5]$", "white",
          lambda t: t["nbInfected"] >= 5),
        ("$\mathrm{E}[nbInfected \;\|\; nbInfected \geq 5]$", "white",
          lambda t: t[t["nbInfected"] >= 5]["nbInfected"])
      ],
      [ # Page: lengthGen1, length5Infected, lengthEpidemy
        ("generation interval", "white",
          lambda t: np.array([generationInterval(t)])),
        ("generation interval (from symptomatic)", "white",
          lambda t: np.array([generationInterval(t, 1)])),
        ("generation interval (from asymptomatic)", "white",
          lambda t: np.array([generationInterval(t, 0)]))
      ],
      [ # Page: lengthGen1, length5Infected, lengthEpidemy
        ("generation interval", "white",
          lambda t: t[t["nbInfected"] > 0]["lengthGen1"]),
        ("generation interval (from symptomatic)", "white",
          lambda t: t[(t["nbInfected"] > 0)
                    & (t["startSympt"] == 1)]["lengthGen1"]),
        ("generation interval (from asymptomatic)", "white",
          lambda t: t[(t["nbInfected"] > 0)
                    & (t["startSympt"] == 0)]["lengthGen1"]),
      ],
      [ # Page: length5Infected, length5Infected, lengthEpidemy
        ("$\mathrm{E}[length\,until\,5\,infected \;\|\; nbInfected \geq 5]$", "white",
          lambda t: t[t["nbInfected"] >= 5]["length5Infected"]),
        ("$\mathrm{E}[length\,until\,10\,infected \;\|\; nbInfected \geq 10]$", "white",
          lambda t: t[t["nbInfected"] >= 10]["length10Infected"]),
        ("$\mathrm{E}[length\,until\,end\,of\,epidemy\;\|\; nbInfected \geq 5]$", "white",
          lambda t: t[t["nbInfected"] >= 5]["lengthEpidemy"])
      ],
      [ # Page: contanimations
        ("symptomatic contaminations", "white",
          lambda t: t[t["nbInfected"] > 0]["nbInfectedFromS"]
                  / t[t["nbInfected"] > 0]["nbInfected"]),
        ("asymptomatic contaminations", "white",
          lambda t: t[t["nbInfected"] > 0]["nbInfectedFromA"]
                  / t[t["nbInfected"] > 0]["nbInfected"])
      ]
    ]
    
    
    for plot in plots:
      fig, axes = plt.subplots(nrows=1, ncols=len(plot),
        figsize=(len(plot)*sizeplot,sizeplot))
      if len(plot) == 1: axes = [axes]
      
      for ax, (desc, col, fun) in zip(axes, plot):
        ax.patch.set_facecolor(col)
        ax.patch.set_alpha(0.15)
        ax.grid(True, which='major', linestyle="--", color='grey', alpha=.25)
        for strat in filenames:
          vals = list(map(float, values[strat]))
          if key == "s": vals = [v+1.5 for v in vals] # infectious period
          cols = [fun(t) for t in tables[strat]]
          means = np.array([c.mean() for c in cols])
          errs = np.array([incertitude(c) for c in cols])
          ax.errorbar(vals, means, yerr=errs,
            label=strat_description[strat], color=strat_color[strat])  
          ax.fill_between(vals, means-errs, means+errs,
            color=strat_color[strat], alpha=0.2)
        ax.set_ylabel(desc)
        ax.set_xlabel(param_description[key])
        if key in ["pk", "ek", "sk"]:
          ax.set_xscale("log")
        default = float(getParamDefault(param, key))
        if key == "s": default += 1.5
        ax.axvline(default, linestyle=":", color="k", linewidth=1)
        ax.legend()
      fig.tight_layout()
      pdf.savefig()
      plt.close()
  
def graph_gamma(ax, means, shapes, discrete=False):
  if min(means) == max(means):
    norm = mpl.colors.LogNorm(vmin=min(shapes), vmax=max(shapes))
    colors = [plt.cm.viridis(norm(s)) for s in shapes]
  else:
    norm = mpl.colors.Normalize(vmin=min(means), vmax=max(means))
    colors = [plt.cm.viridis(norm(m)) for m in means]
  X = sorted(list(np.linspace(0, 2*max(means), 200)) + means)
  ax.set_xlim((X[0], X[-1]))
  for m,k,c in zip(means, shapes, colors):
    rv = gamma(k, scale=m/k)
    lbl = "Gamma(mean=%s, k=%s)" % (str(m),str(k))
    ax.plot(X, [rv.pdf(x) for x in X], label=lbl, color=c)
    if discrete:
      bins = genBins(int(X[0]), int(X[-1]+1))
      ax.plot(X, [rv.pdf(x) for x in X], alpha=0.5, color=c)
      ax.hist([ int(v+random.random()) for v in rv.rvs(100000) ],
        bins=bins, histtype="step", density=True, color=c)
  ax.legend(loc='upper right')

################################################################################

funs = {
  "lengthEpidemy": figure_nbInfected_lengthEpidemy,
  "lengthInfected": figure_lengthInfected,
  "nbInfectedLabel": figure_nbInfectedLabel,
  "generation": figure_generation,
}

if __name__ == "__main__":

  for target, param in targets_plot.items():
    if not os.path.exists(target):
      print("gen", target)
    
      if param["plot"] in funs:
        filename = getFilenameData(param)
        table, description = load_csv(filename)
        fig = funs[param["plot"]](table, description)
        fig.savefig(target)
        plt.close(fig)

      keys = [k for k in param if param[k] == "PARAM"]
      filenames = dict() # trie on keys
      for f,p in targets_data.items():
        if all(param[k] == p[k] or k in keys for k in p):
          d = filenames
          for k in keys:
            if p[k] not in d:
              d[p[k]] = dict()
            d = d[p[k]]
          d[None] = f
      
      if param["plot"] == "days":
        assert(keys == ["strat"])
        figure_days(target, param, filenames)
      
      if param["plot"] == "coupling":
        assert(keys == ["strat"])
        figure_coupling(target, param, filenames)
      
      if param["plot"] == "data":
        assert(len(keys) == 2 and keys[0] == "strat")
        figure_data(target, param, keys[1], filenames)
        
      if param["plot"] == "summary":
        assert(len(keys) == 2 and keys[0] == "strat")
        figure_summary(target, param, keys[1], filenames)
      
      if param["plot"] == "sensitivity":
        assert(len(keys) == 2 and keys[0] == "strat")
        figure_sensitivity(target, param, keys[1], filenames)
      
      if param["plot"] == "sensitivity-R":
        assert(keys == ["strat", "p"])
        fig = figure_sensitivity_R(target, param, keys[1], filenames)
  
