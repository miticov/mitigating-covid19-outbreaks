import sys, re, os, itertools

# targets are "_figures/%.pdf"
targets_figures = [ f[9:-4] for f in sys.argv[1:] ]

# parameters
param_default = {
  "strat": "none",
  "ext-type": "scaling",
  "ext-coeff": "25e-2",
  "d": "1",
  "q": "4e-1",  # 40%
  "a": "5e-1",  # 50%
  "p": {
    "primaryschool": "00090e-5",
    "highschool":    "00350e-5",
    "workplace":     "01010e-5",
    "random_unif":   "00176e-5",
  },
  "pk": "1e-1",    # distrib: exp
  
  # P = 1 + Bernoulli(1/2)
  # I+P ~ Gamma(shape=8, mean=5.2)
  "e": "37e-1", # 3.7 days
  "ek": "5",    # distrib: gamma
  
  # E[H] = 8, P[H>14]~2%, P[H>15]~1%
  "s": "08",    # 8 days
  "sk": "10",   # distrib: gamma
}

param_description = {
  "ext-coeff": "coefficient external graph",
  "d": "number of day at work with symptoms",
  "q": "proba asymptomatic",
  "a": "dampening factor asymptomatic",
  "p": "average of transmission probability",
  "pk": "shape of transmission probability",
  "e": "average of exposed period",
  "ek": "shape of exposed period",
  "s": "average of infectious period", # hardcoded change here
  "sk": "shape of symptomatic period",
}

strat_description = {
  "none": "No Strategy",
  "onoff1d": "On-Off Daily",
  "onoff1w": "On-Off Weekly",
  "rotation1d": "Rotating Daily",
  "rotation1w": "Rotating Weekly",
  "closed": "Full Telecommuting",
  "work": "Work",
  "onoff-4-10": "onoff-4-10",
  "rotation-4-10": "rotation-4-10",
}

param_all = {
  "graph": [ # filename
    "primaryschool", "highschool", "workplace", "random_unif"
  ],
  "strat": [ # temporal strategy
    "none", "onoff1d", "onoff1w", "rotation1d", "rotation1w", "closed",
    #"work", "onoff-4-10", "rotation-4-10"
  ],
  "ext-type": [ # type external graph
    "scaling", "sorting", "complete"
  ],
  "ext-coeff": [ # coeff external graph temporal strategy
    "0", "1e-1", "2e-1", "25e-2", "3e-1", "4e-1", "5e-1",
  ],
  "d": [ # nb day at work with symptoms
    "0", "1", "2", "3", "4", "5"
  ], 
  "q": [ # proba asymptomatic
    "0", "1e-1", "2e-1", "3e-1", "4e-1", "5e-1",
    "6e-1", "7e-1", "8e-1", "9e-1", "1"
  ], 
  "a": [ # dampening factor asymptomatic
    "0", "1e-1", "2e-1", "3e-1", "4e-1", "5e-1", "6e-1", "8e-1", "1"
  ], 
  "p": { # proba infection
    "primaryschool": [
      "03e-4", "05e-4", "07e-4", "09e-4", "11e-4",
      "13e-4", "15e-4", "17e-4", "19e-4", "21e-4",
    ],
    "highschool": [
      "10e-4", "20e-4", "30e-4", "40e-4", "50e-4",
      "60e-4", "70e-4", "80e-4", "90e-4", "100e-4",
    ],
    "workplace": [
      "030e-4", "060e-4", "090e-4", "120e-4", "150e-4",
      "180e-4", "210e-4", "240e-4", "270e-4", "300e-4",
    ],
    "random_unif": [
      "06e-4", "10e-4", "14e-4", "18e-4", "22e-4",
      "26e-4", "30e-4", "34e-4", "38e-4",
    ]
  },
  "pk": [ # shape parameter
    "1e-1", "2e-1", "5e-1", "1", "2", "5", "10", "20", "50", "100"
  ],
  "e": [ # length exposed
    "20e-1", "25e-1", "30e-1", "35e-1", "40e-1", "45e-1", "50e-1"
  ],
  "ek": [ # shape parameter
    "1e-1", "2e-1", "5e-1", "1", "2", "5", "10", "20", "50", "100"
  ],
  "s": [ # length symptomatic
    "05", "06", "07", "08", "09", "10", "11"
  ],
  "sk": [ # shape parameter
    "1e-1", "2e-1", "5e-1", "1", "2", "5", "10", "20", "50", "100"
  ]
}


def getParam(param, key):
  if type(param[key]) == dict():
    return param[key][param["graph"]]
  return param[key]

def getParamDefault(param, key):
  global param_default
  result = param_default[key]
  if type(result) == dict:
    result = result[param["graph"]]
  return result

def getParamAll(param, key):
  global param_all
  result = param_all[key]
  if type(result) == dict:
    result = result[param["graph"]]
  return result + [getParamDefault(param, key)]

################################################################################

def getFilenameData(param):
  target = "_data/%s.strat=%s" % (param["graph"], param["strat"])
  for k,v in param.items():
    if k not in ["trace", "graph", "strat", "plot"]:
      if getParamDefault(param, k) != v:
        target += ".%s=%s" % (k,v)
  target += ".csv"
  return target

def getFilenameGraph(param):
  target = "_graph/%s.ext-type=%s" % (param["graph"], param["ext-type"])
  for k,v in param.items():
    if k not in ["trace", "graph", "strat", "plot", "ext-type"]:
      if getParamDefault(param, k) != v:
        target += ".%s=%s" % (k,v)
  target += ".txt"
  return target

def getFilenameTrace(param):
  target = "_trace/"
  for k,v in param.items():
    if k not in ["trace", "graph", "strat", "plot"]:
      if getParamDefault(param, k) != v:
        target += "%s=%s/" % (k,v)
  target += "%s/%s/" % (param["graph"], param["strat"])
  return target

def getFilenamePlot(param):
  target = "_plot/%s.strat=%s" % (param["graph"], param["strat"])
  for k,v in param.items():
    if k not in ["trace", "graph", "strat", "plot"]:
      if getParamDefault(param, k) != v:
        target += ".%s=%s" % (k,v)
  target += ".%s.pdf" % param["plot"]
  return target

################################################################################

targets_data, targets_plot = dict(), dict()

for t in targets_figures:
  param = param_default.copy()
  graph, flags = [], t.split(".")
  for f in flags[:-1]:
    if f.count("=") == 1:
      key,val = f.split("=")
      param[key] = val
    else:
      graph.append(f)
  param["graph"] = ".".join(graph)
  param["plot"] = flags[-1]
  for k in param:
    if type(param[k]) is dict:
      param[k] = param[k][param["graph"]]
  
  param_data, param_plot = dict(), dict()
  for k,v in param.items():
    if v == "ALL":
      param_data[k] = getParamAll(param, k)
      param_plot[k] = getParamAll(param, k)
    elif v == "PARAM":
      param_data[k] = getParamAll(param, k)
      param_plot[k] = [v]
    else:
      param_data[k] = [v]
      param_plot[k] = [v]
  param_data.pop("plot")
  
  for vals in itertools.product(*param_data.values()):
    param = { k:v for k,v in zip(param_data.keys(), vals) }
    targets_data[getFilenameData(param)] = param
  
  for vals in itertools.product(*param_plot.values()):
    param = { k:v for k,v in zip(param_plot.keys(), vals) }
    targets_plot[getFilenamePlot(param)] = param

