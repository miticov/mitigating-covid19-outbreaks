#include <iostream>
#include <sstream>
#include <fstream>
#include <cstdlib>
#include <vector>
#include <map>
#include <string>
#include <random>
#include <algorithm>
using namespace std;

#include "covid.hpp"

void error(string msg)
{
  cerr << "Error: " << msg << endl;
  exit(1);
}

// Read function

map<int,int> ids;
map<string,vector<int>> labels;

void read_graph(istream &in)
{
  int nbPersons, nbEdges;
  in >> nbPersons >> nbEdges;
  in.ignore();
  
  persons.resize(nbPersons);
  edges.resize(nbEdges);
  
  for (int idPerson=0; idPerson<nbPersons; idPerson++)
  {
    string line; getline(in, line);
    stringstream person(line);
    int id; string label; double q;
    person >> id >> label >> q;
    
    labels[label].push_back(idPerson);
    persons[idPerson] = Person(id, label, q);
  }
  
  sort(persons.begin(), persons.end(), [](const Person &a, const Person &b) {
    return a.id < b.id;
  });
  
  for (int idPerson=0; idPerson<nbPersons; idPerson++)
    ids[persons[idPerson].id] = idPerson;
  
  for (int i=0; i<nbEdges; i++)
  {
    string line; getline(in, line);
    stringstream edge(line);
    int from, to; double weight;
    vector<double> weights;
    edge >> from >> to;
    while (edge >> weight)
      weights.push_back(weight);
    
    if (ids.count(from) == 0)
      error("unknown person.");
    if (ids.count(to) == 0)
      error("unknown person.");
    from = ids[from];
    to = ids[to];
    
    edges[from].push_back(Edge(from, to, weights));
  }
}

void debug_gamma(string v, double k, double e)
{
  double s = e / k;
  cerr << v << " = Gamma(k=" << k << ", s=" << s << ")";
  cerr << ", E[" << v << "] = " << e;
  cerr << ", Var[" << v << "] = " << k*s*s << endl;
}

bool fileexists(string filename) {
  ifstream f(filename);
  return f.good();
}

template<class DISTRIB>
void debug_distrib(DISTRIB d)
{
  const int repeat = 100000;
  double v[repeat], mean = 0, var = 0;
  for (int i=0; i<repeat; i++)
    mean += v[i] = d(random_gen);
  mean /= repeat;
  for (int i=0; i<repeat; i++)
    var += (mean - v[i]) * (mean - v[i]);
  var /= repeat;
  cerr << "E = " << mean << ", Var = " << var << endl;
}

int main(int argc, char** argv)
{
  // default args
  double days = 70;
  string data = "", trace = "";
  ofstream out_data, out_trace;
  
  // parse command line
  if (argc % 2 == 0) error("number of parameter should be even.");
  for (int i=1; i<argc; i++)
  {
    if (string(argv[i]) == "-data")
      data = string(argv[++i]);
    else if (string(argv[i]) == "-trace")
      trace = string(argv[++i]);
    else if (string(argv[i]) == "-d")
      param_d = atoi(argv[++i]);
    else if (string(argv[i]) == "-a")
      param_a = atof(argv[++i]);
    else if (string(argv[i]) == "-p")
      param_p = atof(argv[++i]);
    else if (string(argv[i]) == "-pk")
      param_pk = atof(argv[++i]);
    else if (string(argv[i]) == "-e")
      param_e = atof(argv[++i]);
    else if (string(argv[i]) == "-ek")
      param_ek = atof(argv[++i]);
    else if (string(argv[i]) == "-s")
      param_s = atof(argv[++i]);
    else if (string(argv[i]) == "-sk")
      param_sk = atof(argv[++i]);
    else if (string(argv[i]) == "-days")
      days = atoi(argv[++i]);
    else
      error("parameter \"" + string(argv[i]) + "\" unknown.");
  }
  
  using gamma_param = gamma_distribution<double>::param_type;
  random_p.param(gamma_param(param_pk));
  random_e.param(gamma_param(param_ek, param_e / param_ek));
  random_s.param(gamma_param(param_sk, param_s / param_sk));
  
  // read graph
  vector<vector<pair<int,double>>> graph;
  read_graph(cin);
  
  // debug
  int N = persons.size(), M = 0;
  for (const auto &v : edges)
    M += v.size();
  cerr << "N = " << N << endl;
  cerr << "M = " << M << endl;
  
  cerr << "---" << endl;
  debug_gamma("p", param_pk, param_p);
  cerr << "continuous: "; debug_distrib([](mt19937 &gen) {
    return random_proba(gen, 1);
  });
  cerr << "---" << endl;
  debug_gamma("e", param_ek, param_e);
  cerr << "continuous: "; debug_distrib(random_e);
  cerr << "rounding:   "; debug_distrib(random_exposed<mt19937>);
  cerr << "---" << endl;
  debug_gamma("s", param_sk, param_s);
  cerr << "continuous: "; debug_distrib(random_s);
  cerr << "rounding:   "; debug_distrib(random_symptomatic<mt19937>);
  cerr << "---" << endl;
  
  // params
  int nbGenerations = 2;
  vector<int> nbInfectedThresholds = {5, 10};
  
  // header
  if (data != "")
  {
    out_data.open(data);
    out_data << "startId,startLabel,startSympt,startDay,lengthEpidemy";
    for (int x: nbInfectedThresholds)
      out_data << ",length" << x << "Infected";
    for (int g=1; g<=nbGenerations; g++)
      out_data << ",lengthGen" << g;
    out_data << ",nbInfectedFromS,nbInfectedFromA";
    for (int g=1; g<=nbGenerations; g++)
      out_data << ",nbInfectedGen" << g;
    for (auto label: labels)
      out_data << ",nbAsympt" << label.first << ",nbSympt" << label.first;
    out_data << ",prevalence";
    for (int d=0; d<14; d++)
      out_data << ",incidenceDay" << d;
    out_data << endl;
  }
  
  // simulate
  
  int seed = -1;
  for (int start=0; start<N; start++)
  {
    cerr << "\r" << start+1 << "/" << N << flush;
    
    for (int d=0; d<days; d++)
    {
      seed++; // each execution has a unique seed
      
      stringstream trace_file;
      trace_file << trace << seed << ".txt";
      bool print_trace = fileexists(trace_file.str());
      bool print_data = data != "";
      
      if (print_data || print_trace)
      {
        day = d;
        reset(start, seed);
        simulate();
        
        // aggregate data
        
        int nbInfected = 0;
        map<string, int> infected[2];
        int nbInfectedFromS = 0, nbInfectedFromA = 0;
        vector<int> nbInfectedGen(nbGenerations+1, 0);
        vector<int> lengthGen(nbGenerations+1, 0);
        vector<int> dateInfected;
        int prevalence = 0;
        int incidence[14] = {};
        for (int i=0; i<N; i++)
        {
          if (persons[i].state)
          {
            prevalence += persons[i].lenProdomal;
            if (persons[i].hasSymptoms)
              prevalence += min(param_d, persons[i].lenSymptomatic);
            else
              prevalence += persons[i].lenSymptomatic;
            if (i != start)
            {
              nbInfected++;
              if (persons[i]._fromSymptoms)
                nbInfectedFromS++;
              else
                nbInfectedFromA++;
              infected[persons[i].hasSymptoms][persons[i].label]++;
              dateInfected.push_back(persons[i]._infection - d);
              incidence[persons[i]._infection % 14]++;
              
              int gen = persons[i]._generation;
              if (gen <= nbGenerations)
              {
                nbInfectedGen[gen]++;
                int j = ids[persons[i]._fromId];
                int delta = persons[i]._infection - persons[j]._infection;
                lengthGen[gen] += delta;
              }
            }
          }
        }
        sort(dateInfected.begin(), dateInfected.end());
          
        // output aggregated data
        if (print_data)
        {
          out_data << persons[start].id;
          out_data << "," << persons[start].label;
          out_data << "," << persons[start].hasSymptoms;
          out_data << "," << d << "," << day-d;
          
          for (auto x: nbInfectedThresholds)
          {
            if (x <= nbInfected)
              out_data << "," << dateInfected[x-1];
            else out_data << ",nan";
          }
          
          for (int g=1; g<=nbGenerations; g++)
          {
            if (nbInfectedGen[g])
              out_data << "," << lengthGen[g]/(double)nbInfectedGen[g];
            else out_data << ",nan";
          }
          
          out_data << "," << nbInfectedFromS;
          out_data << "," << nbInfectedFromA;
          
          for (int g=1; g<=nbGenerations; g++)
            out_data << "," << nbInfectedGen[g];
          
          for (auto label: labels)
          {
            out_data << "," << infected[0][label.first];
            out_data << "," << infected[1][label.first];
          }
          
          out_data << "," << prevalence;
          for (int d=0; d<14; d++)
            out_data << "," << incidence[d];
          out_data << endl;
        }
        
        // trace file
        
        if (print_trace)
        {
          out_trace.open(trace_file.str());
          out_trace << "idFrom idTo symptFrom dayInfect pInfect";
          out_trace << " symptTo lenExposed lenProdomal lenSymptomatic";
          out_trace << endl;
          for (Person person : persons)
          {
            if (person.state)
            {
              out_trace << person._fromId << " " << person.id;
              if (person._fromId == -1)
                out_trace << " ?"; // init
              else if (person._fromSymptoms)
                out_trace << " S";
              else
                out_trace << " A";
              out_trace << " " << person._infection;
              out_trace << " " << person._proba;
              out_trace << (person.hasSymptoms ? " S" : " A");
              out_trace << " " << person.lenExposed;
              out_trace << " " << person.lenProdomal;
              out_trace << " " << person.lenSymptomatic;
              out_trace << endl;
            }
          }
          out_trace.close();
        }
      }
    }
  }
  
  out_data.close();
  
  // debug
  cerr << endl;
}
