none median[nbInfected] = 47.0
[422, 1098, 1428, 2727, 6901, 8478, 10246, 10970, 13715, 14027]
onoff1d median[nbInfected] = 12.0
[131, 644, 1118, 1181, 1199, 1234, 1289, 1370, 1379, 1462]
onoff1w median[nbInfected] = 10.0
[94, 138, 457, 596, 635, 746, 768, 842, 1021, 1105]
rotation1d median[nbInfected] = 8.0
[64, 372, 388, 527, 569, 591, 768, 816, 838, 942]
rotation1w median[nbInfected] = 8.0
[275, 562, 664, 746, 756, 805, 816, 942, 1093, 1128]
closed median[nbInfected] = 6.0
[275, 344, 488, 562, 596, 605, 746, 936, 1181, 1210]
The 70-th percentile vector is 
none          2.0
onoff1d       1.0
onoff1w       1.0
rotation1d    0.0
rotation1w    0.0
closed        0.0
dtype: float64
Execution 1844 is the closest
none          2
onoff1d       1
onoff1w       1
rotation1d    0
rotation1w    0
closed        0
Name: 1844, dtype: int64

The 71-th percentile vector is 
none          2.0
onoff1d       1.0
onoff1w       1.0
rotation1d    1.0
rotation1w    1.0
closed        0.0
dtype: float64
Execution 2914 is the closest
none          2
onoff1d       1
onoff1w       1
rotation1d    1
rotation1w    1
closed        0
Name: 2914, dtype: int64

The 72-th percentile vector is 
none          3.0
onoff1d       1.0
onoff1w       1.0
rotation1d    1.0
rotation1w    1.0
closed        0.0
dtype: float64
Execution 2757 is the closest
none          3
onoff1d       1
onoff1w       1
rotation1d    1
rotation1w    1
closed        0
Name: 2757, dtype: int64

The 73-th percentile vector is 
none          3.0
onoff1d       1.0
onoff1w       1.0
rotation1d    1.0
rotation1w    1.0
closed        0.0
dtype: float64
Execution 2757 is the closest
none          3
onoff1d       1
onoff1w       1
rotation1d    1
rotation1w    1
closed        0
Name: 2757, dtype: int64

The 74-th percentile vector is 
none          4.0
onoff1d       1.0
onoff1w       1.0
rotation1d    1.0
rotation1w    1.0
closed        0.0
dtype: float64
Execution 11774 is the closest
none          4
onoff1d       1
onoff1w       1
rotation1d    1
rotation1w    1
closed        0
Name: 11774, dtype: int64

The 75-th percentile vector is 
none          4.0
onoff1d       1.0
onoff1w       1.0
rotation1d    1.0
rotation1w    1.0
closed        0.0
dtype: float64
Execution 11774 is the closest
none          4
onoff1d       1
onoff1w       1
rotation1d    1
rotation1w    1
closed        0
Name: 11774, dtype: int64

The 76-th percentile vector is 
none          5.0
onoff1d       1.0
onoff1w       1.0
rotation1d    1.0
rotation1w    1.0
closed        0.0
dtype: float64
Execution 10882 is the closest
none          5
onoff1d       1
onoff1w       1
rotation1d    1
rotation1w    1
closed        0
Name: 10882, dtype: int64

The 77-th percentile vector is 
none          5.0
onoff1d       1.0
onoff1w       1.0
rotation1d    1.0
rotation1w    1.0
closed        0.0
dtype: float64
Execution 10882 is the closest
none          5
onoff1d       1
onoff1w       1
rotation1d    1
rotation1w    1
closed        0
Name: 10882, dtype: int64

The 78-th percentile vector is 
none          6.0
onoff1d       2.0
onoff1w       2.0
rotation1d    1.0
rotation1w    1.0
closed        1.0
dtype: float64
Execution 12879 is the closest
none          6
onoff1d       2
onoff1w       2
rotation1d    1
rotation1w    1
closed        1
Name: 12879, dtype: int64

The 79-th percentile vector is 
none          7.0
onoff1d       2.0
onoff1w       2.0
rotation1d    1.0
rotation1w    1.0
closed        1.0
dtype: float64
Execution 23404 is the closest
none          7
onoff1d       2
onoff1w       2
rotation1d    1
rotation1w    1
closed        1
Name: 23404, dtype: int64

The 80-th percentile vector is 
none          8.0
onoff1d       2.0
onoff1w       2.0
rotation1d    1.0
rotation1w    1.0
closed        1.0
dtype: float64
Execution 21624 is the closest
none          8
onoff1d       2
onoff1w       2
rotation1d    1
rotation1w    1
closed        1
Name: 21624, dtype: int64

The 81-th percentile vector is 
none          10.0
onoff1d        2.0
onoff1w        2.0
rotation1d     1.0
rotation1w     1.0
closed         1.0
dtype: float64
Execution 10031 is the closest
none          10
onoff1d        2
onoff1w        2
rotation1d     1
rotation1w     1
closed         1
Name: 10031, dtype: int64

The 82-th percentile vector is 
none          12.0
onoff1d        3.0
onoff1w        2.0
rotation1d     2.0
rotation1w     1.0
closed         1.0
dtype: float64
Execution 47081 is the closest
none          12
onoff1d        3
onoff1w        2
rotation1d     1
rotation1w     1
closed         1
Name: 47081, dtype: int64

The 83-th percentile vector is 
none          14.0
onoff1d        3.0
onoff1w        3.0
rotation1d     2.0
rotation1w     2.0
closed         1.0
dtype: float64
Execution 41812 is the closest
none          15
onoff1d        3
onoff1w        3
rotation1d     2
rotation1w     2
closed         1
Name: 41812, dtype: int64

The 84-th percentile vector is 
none          18.0
onoff1d        3.0
onoff1w        3.0
rotation1d     2.0
rotation1w     2.0
closed         1.0
dtype: float64
Execution 10152 is the closest
none          18
onoff1d        3
onoff1w        3
rotation1d     1
rotation1w     1
closed         1
Name: 10152, dtype: int64

The 85-th percentile vector is 
none          22.0
onoff1d        4.0
onoff1w        3.0
rotation1d     2.0
rotation1w     2.0
closed         1.0
dtype: float64
Execution 14861 is the closest
none          22
onoff1d        3
onoff1w        3
rotation1d     2
rotation1w     2
closed         2
Name: 14861, dtype: int64

The 86-th percentile vector is 
none          28.0
onoff1d        4.0
onoff1w        4.0
rotation1d     2.0
rotation1w     2.0
closed         1.0
dtype: float64
Execution 18601 is the closest
none          28
onoff1d        2
onoff1w        4
rotation1d     2
rotation1w     2
closed         2
Name: 18601, dtype: int64

The 87-th percentile vector is 
none          36.0
onoff1d        4.0
onoff1w        4.0
rotation1d     2.0
rotation1w     2.0
closed         1.0
dtype: float64
Execution 63928 is the closest
none          38
onoff1d        4
onoff1w        4
rotation1d     2
rotation1w     2
closed         1
Name: 63928, dtype: int64

The 88-th percentile vector is 
none          48.0
onoff1d        5.0
onoff1w        5.0
rotation1d     3.0
rotation1w     3.0
closed         1.0
dtype: float64
Execution 3350 is the closest
none          49
onoff1d        4
onoff1w        4
rotation1d     3
rotation1w     3
closed         2
Name: 3350, dtype: int64

The 89-th percentile vector is 
none          62.0
onoff1d        6.0
onoff1w        5.0
rotation1d     3.0
rotation1w     3.0
closed         1.0
dtype: float64
Execution 56163 is the closest
none          62
onoff1d        5
onoff1w        5
rotation1d     2
rotation1w     2
closed         2
Name: 56163, dtype: int64

The 90-th percentile vector is 
none          77.0
onoff1d        7.0
onoff1w        6.0
rotation1d     3.0
rotation1w     3.0
closed         2.0
dtype: float64
Execution 34231 is the closest
none          77
onoff1d        7
onoff1w        6
rotation1d     3
rotation1w     4
closed         3
Name: 34231, dtype: int64

The 91-th percentile vector is 
none          93.0
onoff1d        8.0
onoff1w        7.0
rotation1d     4.0
rotation1w     4.0
closed         2.0
dtype: float64
Execution 40901 is the closest
none          93
onoff1d        6
onoff1w        6
rotation1d     4
rotation1w     4
closed         0
Name: 40901, dtype: int64

The 92-th percentile vector is 
none          108.0
onoff1d         9.0
onoff1w         8.0
rotation1d      4.0
rotation1w      4.0
closed          2.0
dtype: float64
Execution 20365 is the closest
none          108
onoff1d        10
onoff1w         7
rotation1d      3
rotation1w      3
closed          3
Name: 20365, dtype: int64

The 93-th percentile vector is 
none          121.0
onoff1d        11.0
onoff1w         9.0
rotation1d      5.0
rotation1w      5.0
closed          2.0
dtype: float64
Execution 82774 is the closest
none          119
onoff1d        11
onoff1w         8
rotation1d      5
rotation1w      3
closed          3
Name: 82774, dtype: int64

The 94-th percentile vector is 
none          134.0
onoff1d        13.0
onoff1w        11.0
rotation1d      6.0
rotation1w      5.0
closed          3.0
dtype: float64
Execution 85501 is the closest
none          135
onoff1d        13
onoff1w        11
rotation1d      5
rotation1w      5
closed          5
Name: 85501, dtype: int64

The 95-th percentile vector is 
none          145.0
onoff1d        16.0
onoff1w        13.0
rotation1d      7.0
rotation1w      6.0
closed          3.0
dtype: float64
Execution 39636 is the closest
none          146
onoff1d        15
onoff1w        13
rotation1d      5
rotation1w      7
closed          5
Name: 39636, dtype: int64

The 96-th percentile vector is 
none          157.0
onoff1d        21.0
onoff1w        16.0
rotation1d      8.0
rotation1w      8.0
closed          3.0
dtype: float64
Execution 17166 is the closest
none          156
onoff1d        21
onoff1w        14
rotation1d      8
rotation1w      5
closed          0
Name: 17166, dtype: int64

The 97-th percentile vector is 
none          168.0
onoff1d        27.0
onoff1w        20.0
rotation1d     10.0
rotation1w      9.0
closed          4.0
dtype: float64
Execution 50928 is the closest
none          168
onoff1d        27
onoff1w        19
rotation1d      7
rotation1w      7
closed          5
Name: 50928, dtype: int64

The 98-th percentile vector is 
none          181.0
onoff1d        36.0
onoff1w        26.0
rotation1d     13.0
rotation1w     12.0
closed          5.0
dtype: float64
Execution 8534 is the closest
none          185
onoff1d        38
onoff1w        30
rotation1d     14
rotation1w     11
closed          7
Name: 8534, dtype: int64

The 99-th percentile vector is 
none          196.0
onoff1d        55.0
onoff1w        38.0
rotation1d     19.0
rotation1w     17.0
closed          7.0
dtype: float64
Execution 63861 is the closest
none          193
onoff1d        57
onoff1w        45
rotation1d     23
rotation1w     13
closed         12
Name: 63861, dtype: int64

