none median[nbInfected] = 26.0
[397, 423, 497, 573, 664, 2200, 2313, 2365, 2587, 2874]
onoff1d median[nbInfected] = 13.0
[348, 537, 862, 868, 1094, 1185, 1442, 1539, 1632, 1636]
onoff1w median[nbInfected] = 12.0
[294, 339, 382, 390, 451, 477, 807, 809, 868, 1044]
rotation1d median[nbInfected] = 10.0
[292, 344, 408, 415, 434, 477, 497, 1035, 1199, 1332]
rotation1w median[nbInfected] = 9.0
[289, 373, 377, 389, 415, 460, 464, 536, 593, 960]
closed median[nbInfected] = 8.0
[216, 379, 453, 477, 532, 1015, 1035, 1104, 1257, 1396]
The 70-th percentile vector is 
none          3.0
onoff1d       1.0
onoff1w       1.0
rotation1d    1.0
rotation1w    1.0
closed        0.0
dtype: float64
Execution 21308 is the closest
none          3
onoff1d       1
onoff1w       1
rotation1d    1
rotation1w    1
closed        0
Name: 21308, dtype: int64

The 71-th percentile vector is 
none          4.0
onoff1d       1.0
onoff1w       1.0
rotation1d    1.0
rotation1w    1.0
closed        1.0
dtype: float64
Execution 2628 is the closest
none          4
onoff1d       1
onoff1w       1
rotation1d    1
rotation1w    1
closed        1
Name: 2628, dtype: int64

The 72-th percentile vector is 
none          4.0
onoff1d       2.0
onoff1w       1.0
rotation1d    1.0
rotation1w    1.0
closed        1.0
dtype: float64
Execution 2042 is the closest
none          4
onoff1d       2
onoff1w       1
rotation1d    1
rotation1w    1
closed        1
Name: 2042, dtype: int64

The 73-th percentile vector is 
none          5.0
onoff1d       2.0
onoff1w       2.0
rotation1d    1.0
rotation1w    1.0
closed        1.0
dtype: float64
Execution 27312 is the closest
none          5
onoff1d       2
onoff1w       2
rotation1d    1
rotation1w    1
closed        1
Name: 27312, dtype: int64

The 74-th percentile vector is 
none          5.0
onoff1d       2.0
onoff1w       2.0
rotation1d    1.0
rotation1w    1.0
closed        1.0
dtype: float64
Execution 27312 is the closest
none          5
onoff1d       2
onoff1w       2
rotation1d    1
rotation1w    1
closed        1
Name: 27312, dtype: int64

The 75-th percentile vector is 
none          6.0
onoff1d       2.0
onoff1w       2.0
rotation1d    1.0
rotation1w    1.0
closed        1.0
dtype: float64
Execution 2859 is the closest
none          6
onoff1d       2
onoff1w       2
rotation1d    1
rotation1w    1
closed        1
Name: 2859, dtype: int64

The 76-th percentile vector is 
none          7.0
onoff1d       2.0
onoff1w       2.0
rotation1d    1.0
rotation1w    1.0
closed        1.0
dtype: float64
Execution 70253 is the closest
none          7
onoff1d       2
onoff1w       2
rotation1d    1
rotation1w    1
closed        1
Name: 70253, dtype: int64

The 77-th percentile vector is 
none          8.0
onoff1d       2.0
onoff1w       2.0
rotation1d    2.0
rotation1w    2.0
closed        1.0
dtype: float64
Execution 54582 is the closest
none          8
onoff1d       2
onoff1w       2
rotation1d    2
rotation1w    2
closed        1
Name: 54582, dtype: int64

The 78-th percentile vector is 
none          9.0
onoff1d       3.0
onoff1w       3.0
rotation1d    2.0
rotation1w    2.0
closed        1.0
dtype: float64
Execution 23502 is the closest
none          9
onoff1d       3
onoff1w       3
rotation1d    2
rotation1w    2
closed        2
Name: 23502, dtype: int64

The 79-th percentile vector is 
none          10.0
onoff1d        3.0
onoff1w        3.0
rotation1d     2.0
rotation1w     2.0
closed         1.0
dtype: float64
Execution 71916 is the closest
none          10
onoff1d        3
onoff1w        3
rotation1d     2
rotation1w     2
closed         1
Name: 71916, dtype: int64

The 80-th percentile vector is 
none          12.0
onoff1d        3.0
onoff1w        3.0
rotation1d     2.0
rotation1w     2.0
closed         1.0
dtype: float64
Execution 6418 is the closest
none          12
onoff1d        3
onoff1w        3
rotation1d     2
rotation1w     3
closed         2
Name: 6418, dtype: int64

The 81-th percentile vector is 
none          13.0
onoff1d        4.0
onoff1w        3.0
rotation1d     2.0
rotation1w     2.0
closed         1.0
dtype: float64
Execution 16721 is the closest
none          13
onoff1d        4
onoff1w        3
rotation1d     2
rotation1w     3
closed         2
Name: 16721, dtype: int64

The 82-th percentile vector is 
none          15.0
onoff1d        4.0
onoff1w        4.0
rotation1d     3.0
rotation1w     2.0
closed         1.0
dtype: float64
Execution 50310 is the closest
none          15
onoff1d        4
onoff1w        4
rotation1d     2
rotation1w     2
closed         0
Name: 50310, dtype: int64

The 83-th percentile vector is 
none          17.0
onoff1d        5.0
onoff1w        4.0
rotation1d     3.0
rotation1w     3.0
closed         2.0
dtype: float64
Execution 56589 is the closest
none          17
onoff1d        5
onoff1w        4
rotation1d     3
rotation1w     3
closed         1
Name: 56589, dtype: int64

The 84-th percentile vector is 
none          20.0
onoff1d        5.0
onoff1w        5.0
rotation1d     3.0
rotation1w     3.0
closed         2.0
dtype: float64
Execution 32336 is the closest
none          21
onoff1d        5
onoff1w        5
rotation1d     3
rotation1w     3
closed         1
Name: 32336, dtype: int64

The 85-th percentile vector is 
none          22.0
onoff1d        6.0
onoff1w        5.0
rotation1d     3.0
rotation1w     3.0
closed         2.0
dtype: float64
Execution 38536 is the closest
none          22
onoff1d        6
onoff1w        5
rotation1d     3
rotation1w     3
closed         3
Name: 38536, dtype: int64

The 86-th percentile vector is 
none          25.0
onoff1d        7.0
onoff1w        6.0
rotation1d     4.0
rotation1w     4.0
closed         2.0
dtype: float64
Execution 12022 is the closest
none          25
onoff1d        9
onoff1w        6
rotation1d     4
rotation1w     4
closed         0
Name: 12022, dtype: int64

The 87-th percentile vector is 
none          27.0
onoff1d        7.0
onoff1w        7.0
rotation1d     4.0
rotation1w     4.0
closed         2.0
dtype: float64
Execution 30216 is the closest
none          28
onoff1d        7
onoff1w        7
rotation1d     4
rotation1w     4
closed         3
Name: 30216, dtype: int64

The 88-th percentile vector is 
none          30.0
onoff1d        8.0
onoff1w        8.0
rotation1d     5.0
rotation1w     4.0
closed         3.0
dtype: float64
Execution 73872 is the closest
none          30
onoff1d        9
onoff1w        8
rotation1d     5
rotation1w     5
closed         4
Name: 73872, dtype: int64

The 89-th percentile vector is 
none          33.0
onoff1d       10.0
onoff1w        9.0
rotation1d     5.0
rotation1w     5.0
closed         3.0
dtype: float64
Execution 26310 is the closest
none          34
onoff1d       11
onoff1w        9
rotation1d     6
rotation1w     5
closed         4
Name: 26310, dtype: int64

The 90-th percentile vector is 
none          36.0
onoff1d       11.0
onoff1w       10.0
rotation1d     6.0
rotation1w     6.0
closed         3.0
dtype: float64
Execution 27669 is the closest
none          35
onoff1d       12
onoff1w       11
rotation1d     6
rotation1w     6
closed         4
Name: 27669, dtype: int64

The 91-th percentile vector is 
none          39.0
onoff1d       12.0
onoff1w       11.0
rotation1d     7.0
rotation1w     6.0
closed         4.0
dtype: float64
Execution 24231 is the closest
none          40
onoff1d       12
onoff1w       12
rotation1d     6
rotation1w     6
closed         4
Name: 24231, dtype: int64

The 92-th percentile vector is 
none          43.0
onoff1d       14.0
onoff1w       12.0
rotation1d     8.0
rotation1w     7.0
closed         4.0
dtype: float64
Execution 7507 is the closest
none          43
onoff1d       17
onoff1w       12
rotation1d     8
rotation1w     7
closed         7
Name: 7507, dtype: int64

The 93-th percentile vector is 
none          48.0
onoff1d       16.0
onoff1w       14.0
rotation1d     9.0
rotation1w     8.0
closed         5.0
dtype: float64
Execution 4068 is the closest
none          49
onoff1d       16
onoff1w       15
rotation1d     7
rotation1w     7
closed         5
Name: 4068, dtype: int64

The 94-th percentile vector is 
none          53.0
onoff1d       18.0
onoff1w       16.0
rotation1d    10.0
rotation1w     9.0
closed         5.0
dtype: float64
Execution 72516 is the closest
none          54
onoff1d       18
onoff1w       17
rotation1d    10
rotation1w     9
closed         8
Name: 72516, dtype: int64

The 95-th percentile vector is 
none          58.0
onoff1d       21.0
onoff1w       18.0
rotation1d    11.0
rotation1w    11.0
closed         6.0
dtype: float64
Execution 63832 is the closest
none          57
onoff1d       21
onoff1w       18
rotation1d    11
rotation1w    14
closed         5
Name: 63832, dtype: int64

The 96-th percentile vector is 
none          65.0
onoff1d       24.0
onoff1w       21.0
rotation1d    13.0
rotation1w    13.0
closed         7.0
dtype: float64
Execution 75789 is the closest
none          65
onoff1d       21
onoff1w       21
rotation1d    13
rotation1w    11
closed         6
Name: 75789, dtype: int64

The 97-th percentile vector is 
none          73.0
onoff1d       28.0
onoff1w       24.0
rotation1d    16.0
rotation1w    15.0
closed         8.0
dtype: float64
Execution 85097 is the closest
none          72
onoff1d       30
onoff1w       23
rotation1d    18
rotation1w    17
closed        11
Name: 85097, dtype: int64

The 98-th percentile vector is 
none          87.0
onoff1d       34.0
onoff1w       29.0
rotation1d    19.0
rotation1w    19.0
closed        10.0
dtype: float64
Execution 88823 is the closest
none          89
onoff1d       32
onoff1w       34
rotation1d    20
rotation1w    20
closed        12
Name: 88823, dtype: int64

The 99-th percentile vector is 
none          108.0
onoff1d        43.0
onoff1w        37.0
rotation1d     25.0
rotation1w     24.0
closed         14.0
dtype: float64
Execution 81249 is the closest
none          109
onoff1d        36
onoff1w        33
rotation1d     21
rotation1w     27
closed         12
Name: 81249, dtype: int64

