/*
SEIR-like model :
SUSCEPTIBLE --> EXPOSED --> PRODOMAL --> SYMPTOMATIC --> RECOVERED
where INFECTIOUS = PRODOMAL + SYMPTOMATIC
*/

enum State {
  SUSCEPTIBLE = 0,
  EXPOSED = 1,
  PRODOMAL = 2,
  SYMPTOMATIC = 3,
  RECOVERED = 4
};


/******************************************************************************/

// Global variables

int day = 0;
double infectious[] = {0, 0, 1, 1, 0};

// number of day at work with symptoms
int param_d = 1;

// dampening factor asymptomatic
double param_a = 0.5;

// proba transmission
double param_p  = 3e-3;
double param_pk = 1;

// length exposed
double param_e  = 3.7;
double param_ek = 5;

// length symptomatic
double param_s  = 8.0;
double param_sk = 1;

/******************************************************************************/

mt19937 random_gen(42);

// !! Important: random_uniform has no internal state !!
uniform_real_distribution<double> random_uniform(0., 1.);

gamma_distribution<double> random_p;
gamma_distribution<double> random_e;
gamma_distribution<double> random_s;

discrete_distribution<int> random_prodomal({0, 1, 1});

template<class URNG>
double random_proba(URNG& gen, double coeff=1) {
  // Gamma(k = param_pk, mean = coeff * param_p)
  return (coeff * param_p / param_pk) * random_p(gen);
}

template<class URNG>
int random_exposed(URNG& gen) {
  return max(1, (int)(random_e(gen) + random_uniform(gen)));
}

template<class URNG>
int random_symptomatic(URNG& gen) {
  return max(1, (int)(random_s(gen) + random_uniform(gen)));
}


/******************************************************************************/

struct Edge
{
  int from, to;
  vector<double> weights;
  minstd_rand gen;
  
  Edge(int f, int t, vector<double> ws) : from(f), to(t), weights(ws) {}
  
  double getCoeff() const {
    return weights[day % weights.size()];
  }
};

/******************************************************************************/

class Person
{
  public:
    int id; string label;
  
    double probaSymptoms, probaInfection;
    bool hasSymptoms;
    State state; int countdown;                   // state
    int lenExposed, lenProdomal, lenSymptomatic;  // length of each period
    int _fromId = -1, _fromState = -1, _fromSymptoms = -1;
    int _infection = 0, _generation = -1;
    double _proba = 1;
  
    Person() : id(-1), label("None"), probaSymptoms(0.8) { reset(); }
    Person(int i, string l, double q) : id(i), label(l), probaSymptoms(q) { reset(); }
    void reset()
    {
      state = SUSCEPTIBLE;
      countdown = -1;
      probaInfection = 0;
      
      // coupling executions
      hasSymptoms = random_uniform(random_gen) < probaSymptoms;
      lenExposed = random_exposed(random_gen);
      lenProdomal = random_prodomal(random_gen);
      lenSymptomatic = random_symptomatic(random_gen);
    }
    
    void infect(const Person &from)
    {
      if (state == SUSCEPTIBLE)
      {
        countdown = 0;
        _infection = day;
        _fromId = from.id;
        _fromState = from.state;
        _fromSymptoms = from.hasSymptoms;
        _proba = from.probaInfection;
        _generation = from._generation + 1;
      }
    }
    
    double probaInfectionAvg()
    {
      return infectious[state] * (hasSymptoms ? 1.0 : param_a);
    }
    
    void nextDay()
    {
      while (countdown == 0)
      {
        switch(state)
        {
          case SUSCEPTIBLE:
            state = EXPOSED;
            countdown = lenExposed;
            break;
          
          case EXPOSED:
            state = PRODOMAL;
            countdown = lenProdomal;
            break;
          
          case PRODOMAL:
            state = SYMPTOMATIC;
            countdown = lenSymptomatic;
            break;
         
          case SYMPTOMATIC:
            state = RECOVERED;
            countdown = -1;
            break;
          
          case RECOVERED:
            break;
        }
      }
      
      if (countdown > 0)
        countdown--;
      
      probaInfection = random_proba(random_gen, probaInfectionAvg());
    }
};


/******************************************************************************/

const Person outside;
vector<Person> persons;
vector<vector<Edge>> edges;

inline void contact(Edge &e)
{
  double infect = persons[e.from].probaInfection;
  double weight = e.getCoeff();
  double proba = 1 - pow(1 - infect, weight);
  if (random_uniform(e.gen) < proba)
    persons[e.to].infect(persons[e.from]);
}


void reset(int start = -1, int seed = 42)
{
  random_gen.seed(seed);
  random_uniform.reset();
  random_p.reset();
  random_e.reset();
  random_s.reset();
  random_prodomal.reset();

  for (int i=0; i<(int)persons.size(); i++)
  {
    persons[i].reset();
    for (Edge &e : edges[i])
      e.gen.seed(random_gen());
  }
  if (start != -1)
  {
    persons[start].infect(outside);
    persons[start]._infection--;
    persons[start].nextDay();
  }
}

void simulate()
{
  while (true)
  {
    int count[5] = {};
    for (int i=0; i<(int)persons.size(); i++)
    {
      count[persons[i].state]++;
      if (infectious[persons[i].state])
        if (persons[i].state != SYMPTOMATIC || !persons[i].hasSymptoms
         || persons[i].countdown >= persons[i].lenSymptomatic - param_d)
          for (Edge &e : edges[i])
            if (e.getCoeff())
              contact(e);
    }
    
    if (count[1] + count[2] + count[3] == 0)
      break;
    
    day++;
    for (int i=0; i<(int)persons.size(); i++)
      persons[i].nextDay();
  }
}
