import os
from utils import *
from plot import load_csv
from run import job
import pandas as pd
import numpy as np

def getDefault(graph, strat):
  global param_default
  param = dict(param_default)
  param["graph"] = graph
  param["strat"] = strat
  for k in param:
    if type(param[k]) == dict:
      param[k] = param[k][graph]
  return param

def dist(a, b, ord=1):
  return np.linalg.norm(a-b, ord)

for graph in param_all["graph"]:
  
  # create directory and read data
  nbInfected = pd.DataFrame()
  for strat in param_all["strat"]:
    param = getDefault(graph, strat)
    trace = getFilenameTrace(param)
    os.makedirs(trace, exist_ok = True)
    data = getFilenameData(param)
    table, description = load_csv(data)
    nbInfected[strat] = table["nbInfected"]
  
  nbInfected.to_csv("_trace/%s/nbInfected.csv" % graph)
  ids = []
  
  with open("_trace/%s/description.txt" % graph, "w") as f_desc:
  
    #"""
    # compute medians
    for strat in param_all["strat"]:
      t = nbInfected[strat]
      med = t[t >= 5].median()
      l = t[t == med][:10].index #.sample(n=10, random_state=42).index
      ids += list(l)
      print(strat, "median[nbInfected] =", med, file=f_desc)
      print(list(l), file=f_desc)
    #"""
    
    #"""
    # compute percentiles
    for p in range(70, 100, 1):
      v = pd.Series(dtype=int)
      for strat in param_all["strat"]:
        v[strat] = nbInfected[strat].quantile(p / 100)
      if any(v):
        best = min((dist(v,r),i) for i,r in nbInfected.iterrows())
        ids.append(best[1])
        print("The %d-th percentile vector is " % p, file=f_desc)
        print(v, file=f_desc)
        print("Execution %d is the closest" % best[1], file=f_desc)
        print(nbInfected.iloc[best[1]], file=f_desc)
        print(file=f_desc)
    #"""
  
  # create file
  for i in ids:
    for strat in param_all["strat"]:
      f = getFilenameTrace(getDefault(graph, strat))
      open(f + "%d.txt" % i, "a").close()

for param in targets_data.values():
  isDefault =  all(y == getParamDefault(param, x)
                   for x,y in param.items()
                   if x not in ["graph", "strat", "ext-type"])
  if isDefault:
    job(param, mode="trace")

