from subprocess import Popen, PIPE
from multiprocessing import Pool, TimeoutError
from time import time
import os, random, math, functools, sys
import numpy as np

from utils import *

nbProcess = 8

################################################################################

def read_graph(filename):
  if nbProcess == 1: print("loading", filename)
  persons, contacts = dict(), dict()
  with open(filename) as f:
    data = [ l.strip().split() for l in f.readlines() ]
    data = [ (int(id1), int(id2), float(weight), label1, label2)
             for id1, id2, weight, label1, label2 in data ]
    data += [ (id2, id1, weight, label2, label1)
             for id1, id2, weight, label1, label2 in data ]
    for id1, id2, weight, label1, label2 in data:
      if id1 not in persons:
        persons[id1] = label1
        contacts[id1] = dict()
      contacts[id1][id2] = weight
  return persons, contacts

def run(instance, data, trace, param, repeat=14):
  global nbProcesses
  command = ["./main", "-days", str(repeat), "-data", data, "-trace", trace]
  for k,v in param.items():
    if k in ["a", "d", "p", "pk", "e", "ek", "s", "sk"]:
      command += ["-" + k, v ]
  start = time()
  process = Popen(command, stdin=PIPE, stderr=(PIPE if nbProcess > 1 else None))
  out, err = process.communicate(instance.encode())
  if nbProcess == 1:
    print("done in %fs" % (time() - start))

################################################################################

def ppcm(*lst):
  res = 1
  for l in lst:
    res = res * l // math.gcd(res, l)
  return res

def debug_graph(contacts):
  degree = np.array([ len(c) for c in contacts.values() ])
  weight = np.array([ sum(c.values()) for c in contacts.values() ])
  print("Degree: mean =", degree.mean(),
        "min =", degree.min(),
        "max =", degree.max())
  print("Weight: mean =", weight.mean(),
        "min =", weight.min(),
        "max =", weight.max())

def external_scaling(coeff, contacts):
  return { p1: { p2: coeff * w
           for p2, w in contacts[p1].items() }
           for p1 in contacts }

def external_complete(coeff, contacts):
  weight = sum(sum(contacts[p].values()) for p in contacts)
  coeff *= weight / len(contacts) / (len(contacts) - 1)
  return { p1: { p2: coeff
           for p2 in contacts if p2 != p1 }
           for p1 in contacts }

def external_sorting(coeff, contacts):
  result = { p: dict() for p in contacts }
  for p1 in contacts:
    budget = coeff * sum(contacts[p1].values())
    for p2, w in sorted(contacts[p1].items(), key=lambda x:-x[1]):
      w = min(w, budget)
      if w > 0: result[p1][p2] = w
      budget -= w
  for p1 in result:
    for p2 in result:
      if p1 < p2:
        w1 = result[p1][p2] if p2 in result[p1] else 0
        w2 = result[p2][p1] if p1 in result[p2] else 0
        w = (w1 + w2) / 2
        if w > 0:
          result[p1][p2] = w
          result[p2][p1] = w
  return result

def strategy_sequence(repeat, value):
  l = ppcm(14, repeat) # 14 must divide l
  return [(i//repeat%2 == value) and (i%7 not in [5,6]) for i in range(l)]

def strategy_onoff(repeat, persons):
  return { p: strategy_sequence(repeat, 0) for p in persons }

def strategy_rotation(repeat, persons):
  random.seed(42)
  S = set(random.sample(persons.keys(), len(persons) // 2))
  return { p: strategy_sequence(repeat, p in S) for p in persons }
  
def strategy_none(persons):
  return { p: [True] * 5 + [False] * 2 for p in persons }

def strategy_closed(persons):
  return { p: [False] * 7 for p in persons }

################################################################################

graphs = {
  "random_unif":   ["00"],
  "primaryschool": ["00", "01"],
  "highschool":    ["00", "01", "02", "03", "04", None, None],
  "workplace":     ["00", "01", "02", "03", "04", None, None,
                    "07", "08", "09", "10", "11", None, None],
}

def job(param, mode="default"):
  global graphs, nbProcess
  assert(mode in ["default", "trace"])
  
  target = getFilenameData(param)
  trace = getFilenameTrace(param)
  
  if mode == "trace" or not os.path.exists(target):
    if nbProcess == 1: print()
    
    if mode == "default":
      print("gen", target)
      #trace = ""
    if mode == "trace":
      print("trace", trace)
      target = ""
    
    # load graphs
    persons = dict()
    internal = []
    for day in graphs[param["graph"]]:
      if day == None:
        internal.append(None)
      else:
        filename = "../graphs/%s/%s.graph" % (param["graph"], day)
        graph = read_graph(filename)
        for p,l in graph[0].items():
          if p in persons:
            assert(persons[p] == l)
          else:
            persons[p] = l
        internal.append(graph[1])
    
    # average graph
    average = { p : dict() for p in persons }
    for p1 in persons:
      for p2 in persons:
        w = []
        for contacts in internal:
          if contacts != None:
            if p1 in contacts and p2 in contacts[p1]:
              w.append(contacts[p1][p2])
            else:
              w.append(0)
        if any(w):
          average[p1][p2] = sum(w) / len(w) 
    
    # external graph
    external_coeff = float(param["ext-coeff"])
    external_fun = {
      "scaling" : external_scaling,
      "sorting" : external_sorting,
      "complete" : external_complete,
    }[param["ext-type"]]
    external = external_fun(external_coeff, average)
    
    # export external graph, for visualization
    with open(getFilenameGraph(param), "w") as f:
      for p1 in external:
        for p2 in external[p1]:
          if p1 < p2:
            f.write("%d %d %f %s %s\n" % (p1, p2, external[p1][p2],
                                          persons[p1], persons[p2]))
    
    
    # compute when each person is here
    if nbProcess == 1: print("computing strategy...")
    strategy = None
    if param["strat"] == "work":
      strategy = { p: [True] for p in persons }
      internal = [average]
    if param["strat"] == "onoff-4-10":
      strategy = { p: [True] * 4 + [False] * 10 for p in persons }
      internal = [average]
    if param["strat"] == "rotation-4-10":
      random.seed(42)
      S = set(random.sample(persons.keys(), len(persons) // 2))
      strategy = { p : ([True] * 4 + [False] * 10 if p in S else
       [False] * 7 + [True] * 4 + [False] * 3) for p in persons }
      internal = [average]
    if param["strat"] == "none":
      strategy = strategy_none(persons)
    if param["strat"] == "closed":
      strategy = strategy_closed(persons)
    if param["strat"] == "onoff1d":
      strategy = strategy_onoff(1, persons)
    if param["strat"] == "onoff1w":
      strategy = strategy_onoff(7, persons)
    if param["strat"] == "rotation1d":
      strategy = strategy_rotation(1, persons)
    if param["strat"] == "rotation1w":
      strategy = strategy_rotation(7, persons)
    assert(strategy != None)
    
    # combine graphs
    if nbProcess == 1: print("combining graphs...")
    contacts = []
    for p1 in persons:
      for p2 in persons:
        l, l1, l2 = len(internal), len(strategy[p1]), len(strategy[p2])
        weights = []
        for d in range(ppcm(l, l1, l2)):
          w = 0
          if p1 in external and p2 in external[p1]:
            w = external[p1][p2]
          if strategy[p1][d%l1] and strategy[p2][d%l2]:
            if p1 in internal[d%l] and p2 in internal[d%l][p1]:
              w += internal[d%l][p1][p2]
          weights.append(w)
        if any(weights):
          contacts.append((p1, p2, weights))
    
    # generate instance
    if nbProcess == 1: print("generating instance...")
    graph = "%d %d\n" % (len(persons), len(contacts))
    for i in persons:
      graph += "%d %s %f\n" % (i, persons[i], 1-float(param["q"]))
    for id1, id2, weights in contacts:
      graph += "%d %d %s\n" % (id1, id2, " ".join(map(str, weights)))
    
    # run external simulation
    run(graph, target, trace, param, repeat=20*14)


if __name__ == "__main__":

  print("This step can take up to 10h, do you want to proceed? [Y/N]")
  if input() != "Y": sys.exit(1)
  
  if nbProcess > 1:
    with Pool(processes=nbProcess) as pool:
      pool.map_async(job, targets_data.values())
      pool.close()
      pool.join()
  else:
    for v in targets_data.values():
      job(v)

