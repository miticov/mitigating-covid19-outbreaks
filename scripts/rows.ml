(* Author : Laurent Viennot, Inria 2020. *)

(** Read from a channel an array of strings (same number of strings per line). *)
let read_rows ?(sep='\t') inp =
  let rows = ref [] and ncols = ref 0 in
  (try
    while true do
      let r = input_line inp in
      let r = String.split_on_char sep r in
      if !rows = [] then ncols := List.length r;
      if !ncols <> List.length r then
        invalid_arg "different number of strings in a line";
      rows := r :: !rows
    done
   with End_of_file -> ());
  List.rev !rows

let print_rows ?(sep='\t') out rows =
  List.iter (fun r ->
      List.iteri (fun i s ->
          if i <> 0 then output_char out sep;
          output_string out s;
        ) r;
      output_char out '\n';
    ) rows

let print_rows_dbg ?(sep='\t') out rows =
  List.iter (fun r ->
      List.iteri (fun i s ->
          if i <> 0 then output_char out sep;
          output_char out '>';
          output_string out s;
          output_char out '<';
        ) r;
      output_char out '\n';
    ) rows

let check_rows_format rows coltypes =
  let check_val v t =
    match t with
    | "s" | "string" -> true, "ok"
    | "i" | "int" ->
       (try ignore(int_of_string v); true, "ok" with _ -> false, "not int")
    | "f" | "float" ->
       (try ignore(float_of_string v); true, "ok" with _ -> false, "not float")
    | _ -> false, Printf.sprintf "type %s unknown" t
  in
  let line = ref 0 in
  let fail col row v t msg =
    List.iter (Printf.eprintf "'%s'\t") row;
    Printf.eprintf "\nFormat error: %s, in row %d above at column %d "
                   msg !line col;
    Printf.eprintf "(got '%s' for '%s')\n" v t;
    failwith "wrong format"
  in
  let check_row row ctyp =
    let rec iter c r ct =
      match r, ct with
      | [], [] -> ()
      | v :: _, [] -> fail c row v "" "too many columns"
      | [], t :: _ -> fail c row "" t "missing column"
      | v :: r, t :: ct ->
         let ok, msg = check_val v t in
         if not ok then fail c row v t msg;
         iter (c+1) r ct
    in iter 0 row ctyp
  in
  List.iter (fun r ->
      incr line;
      check_row r coltypes
    ) rows
  
let ncols rows =
  match rows with
  | r :: _ -> List.length r
  | _ -> invalid_arg "ncols: no rows"

let other_cols cols rows =
  let l = ref [] in
  for i = 0 to ncols rows - 1 do
    if not (List.mem i cols) then l := i :: !l
  done;
  List.rev !l

let row i rows = List.nth rows i
  
let cell i j rows =
  List.nth (List.nth rows i) j
  
let drop cols rows =
  if rows = [] then [] else
    let keep_cols = other_cols cols rows in
    List.rev_map (fun r ->
        let r = Array.of_list r in
        let k = ref [] in
        List.iter (fun i -> k := r.(i) :: !k) keep_cols;
        List.rev !k
      ) (List.rev rows)

let swap j j' rows =
  if rows = [] then [] else
    List.rev_map (fun r ->
        let r = Array.of_list r in
        let tmp = r.(j) in
        r.(j) <- r.(j');
        r.(j') <- tmp;
        Array.to_list r
      ) (List.rev rows)

let column j rows =
  let dcols = other_cols [j] rows in
  List.flatten (drop dcols rows)

let rank compare k rows =
  let r = ref [] in
  for j = ncols rows - 1 downto 0 do
    let c = column j rows in
    let c = List.sort compare c in
    let n = List.length c in
    let k = if k >= n then n - 1 else k in
    r := (List.nth c k) :: !r
  done;
  [!r]

  
let insert f col rows =
  if rows = [] then [] else 
    let ncols = ncols rows in
    let col = if col < 0 then ncols + 1 + col else col in
    let i = ref (List.length rows) in (* row number *)
    List.rev_map (fun r ->
        decr i;
        let r = Array.of_list r in
        let l = ref [] in
        for i = Array.length r - 1 downto col do
          l := r.(i) :: !l
        done;
        l := (f !i col) :: !l;
        for i = col - 1 downto 0 do
          l := r.(i) :: !l
        done;
        !l
      ) (List.rev rows)

let dup col rows =
  if rows = [] then [] else 
    List.rev_map (fun r ->
        let r = Array.of_list r in
        let l = ref [] in
        for i = Array.length r - 1 downto col do
          l := r.(i) :: !l
        done;
        l := r.(col) :: !l;
        for i = col - 1 downto 0 do
          l := r.(i) :: !l
        done;
        !l
      ) (List.rev rows)

let filter pred col rows =
  let out = ref [] in
  List.iter (fun r ->
      if pred (List.nth r col) then out := r :: !out
    ) (List.rev rows);
  !out
  
let map_cols (f : string -> string) cols rows =
  List.rev_map (fun r ->
      let r = Array.of_list r in
      List.iter (fun i -> r.(i) <- f r.(i)) cols;
      Array.to_list r
    ) (List.rev rows)
  
let map_2cols (f : string -> string -> string * string) j j' rows =
  List.rev_map (fun r ->
      let r = Array.of_list r in
      let s = r.(j) and s' = r.(j') in
      let s, s' = f s s' in
      r.(j) <- s; r.(j') <- s';
      Array.to_list r
    ) (List.rev rows)
  
let aggregate val_of_string val_to_string val_combine aggr_cols rows =
  if rows = [] then [] else
  let ncols = ncols rows in
  let keep_cols = other_cols aggr_cols rows in
  let h = Hashtbl.create 128 in
  let keys = ref [] in (* try to preserve order of rows *)
  let combine k a =
    try
      let a' = Hashtbl.find h k in
      let rec merge a a' =
        match a, a' with
        | [], [] -> []
        | v::a, v'::a' -> val_combine v v' :: merge a a'
        | _ -> assert false
      in
      let a' = merge a' a in
      Hashtbl.replace h k a'
    with Not_found ->
      keys := k :: !keys;
      Hashtbl.add h k a
  in
  List.iter (fun r ->
      let r = Array.of_list r in
      let a = ref [] and k = ref [] in
      List.iter (fun i -> k := r.(i) :: !k) keep_cols;
      List.iter (fun i -> a := val_of_string r.(i) :: !a) aggr_cols;
      combine (List.rev !k) (List.rev !a)
    ) rows;
  let out = ref [] in
  List.iter (fun k ->
      let a = Hashtbl.find h k in
      let r = Array.make ncols "" in
      let set cols vals =
        let cv = List.combine cols vals in
        List.iter (fun (i, v) -> r.(i) <- v) cv
      in
      set keep_cols k;
      set aggr_cols (List.map val_to_string a);
      out := (Array.to_list r) :: !out
    ) !keys;
  !out

let aggregate_int =
  let val_of_string = int_of_string and val_to_string = string_of_int in
  aggregate val_of_string val_to_string

let aggregate_string =
  let id (s : string) = s in
  aggregate id id

let aggregate_float_safe =
  let val_of_string s = try float_of_string s with _ -> 0.
  and val_to_string = string_of_float in
  aggregate val_of_string val_to_string

let join_id
      ?(default=fun j r -> Printf.eprintf "id %s\n" r.(j); raise Not_found)
      ?(subarray=fun r' -> r')
      rows j j' rows' =
  if rows = [] then [] else
  let row'_by_id = Hashtbl.create (List.length rows') in
  List.iter (fun r' ->
      let r' = Array.of_list r' in
      Hashtbl.add row'_by_id r'.(j') (subarray r')
    ) rows';
  let nc = ncols rows and nc' = ncols rows' in
  let rows =
    List.rev_map (fun r ->
        let r = Array.of_list r in
        let r' =
          try Hashtbl.find row'_by_id r.(j)
          with Not_found -> default j r
        in
        let r'' = Array.make (nc + nc' - 1) r.(j) in
        for k = 0 to nc - 1 do r''.(k) <- r.(k) done;
        for k = 0 to nc' - 1 do
          if k < j' then r''.(nc + k) <- r'.(k)
          else if k > j' then r''.(nc + k - 1) <- r'.(k)
        done;
        (*
        let noprt t = () and prt t =
          Printf.eprintf "[ ";
          for i = 0 to Array.length t - 1 do
            Printf.eprintf ">%s<, " t.(i);
          done;
          Printf.eprintf " ]\n"; flush stderr;
        in
        prt r; prt r'; prt r'';
         *)
        Array.to_list r''
      ) (List.rev rows)
  in
  List.rev rows
  
type contact = {
    t : int;
    u : int;
    v : int;
  }

let contact_of_list l = match l with
  | t :: u :: v :: _ -> let i = int_of_string in { t=i t; u=i u; v=i v; }
  | _ -> invalid_arg "contact_of_list expects a list of three integers or more"
  
let contacts_with_duration ?(det_int=20) (* frequency of contact detection *)
                           ?(cont_thr=40) (* maximum allowed time between detections within a long contact *)
                           rows =
  let sym = ref [] in
  List.iter (fun l ->
      let c = contact_of_list l in
      sym := c :: { t=c.t; u=c.v; v=c.u; } :: !sym;
    ) rows;
  let rows = Array.of_list !sym in
  Array.sort (fun c c' ->
      if c.u <> c'.u then compare c.u c'.u
      else if c.v <> c'.v then compare c.v c'.v
      else compare c.t c'.t
    ) rows;
  let n = ref 0 and m = ref 0 and mc = ref 0 and md = ref 0 in
  let u = ref 0 and v = ref 0 and t = ref 0 and last = ref 0 and nc = ref 0 in
  let out = ref [] in
  (* u v start duration number_of_contacts_detected *)
  let out_edge () =
    let r = [!t; !last; !u; !v; !last - !t + det_int; !nc] in
    let r = List.map string_of_int r in
    out := r :: !out;
  in
  let edge c =
    out_edge();
    u := c.u; v := c.v; t := c.t; last := c.t; nc := 1; 
  in
  Array.iter (fun c ->
      incr md;
      if !n = 0
      then (incr n; u := c.u; v := c.v; t := c.t; last := c.t; nc := 1;) 
      else if c.u <> !u then (incr n; edge c)
      else if c.v <> !v then (incr m; edge c)
      else if c.t > !last + cont_thr then (incr mc; edge c)
      else (last := c.t; incr nc)
    ) rows;
  out_edge();
  Printf.eprintf "%d nodes %d edges %d contacts %d detections\n"
                 !n (!m/2) (!mc/2) (!md/2);
  flush stderr;
  List.sort compare !out

  
let main () =
  let rows = ref (read_rows stdin) in
  let icom = ref 0 in
  let arg_string () =
    incr icom;
    Sys.argv.(!icom)
  in
  let arg_string_list () =
    String.split_on_char ',' (arg_string ())
  in
  let arg_int () =
    int_of_string (arg_string ())
  in
  let arg_int_list () =
    List.map int_of_string (arg_string_list ())
  in
  let arg_cols = arg_int_list in
  while !icom + 1 < Array.length Sys.argv do
    incr icom;
    match Sys.argv.(!icom) with
    | "help" ->
       Printf.eprintf "Usage: %s [command] \n" Sys.argv.(0);
       flush stderr;
       exit(1)
    | "format" ->
       let coltypes = arg_string_list () in
       check_rows_format !rows coltypes
    | "cell" ->
       let i = arg_int () in
       let j = arg_int () in
       rows := [[cell i j !rows]]
    | "titles" ->
       let titles = arg_string_list () in
       rows := titles :: !rows
    | "contacts_with_duration" ->
       let det_int = arg_int () in
       let cont_thr = arg_int () in
       rows := contacts_with_duration ~det_int ~cont_thr !rows
    | "sum_int" ->
       rows := aggregate_int (+) (arg_cols ()) !rows
    | "sum_float" ->
       rows := aggregate_float_safe (+.) (arg_cols ()) !rows
    | "col_eq" ->
       let eq s s' = if s = s' then "1", s' else "0", s' in
       let j = arg_int () in
       let j' = arg_int () in
       rows := map_2cols (eq) j j' !rows
    | "col_sum_int" ->
       let sum s s' =
         let ios = int_of_string in
         string_of_int ((ios s) + (ios s')), s'
       in
       let j = arg_int () in
       let j' = arg_int () in
       rows := map_2cols (sum) j j' !rows
    | "col_or" ->
       let oneor s s' = if s = "1" || s' = "1" then "1", s' else "0", s' in
       let j = arg_int () in
       let j' = arg_int () in
       rows := map_2cols oneor j j' !rows
    | "swap" ->
       let j = arg_int () in
       let j' = arg_int () in
       rows := swap j j' !rows
    | "dup" ->
       let j = arg_int () in
       rows := dup j !rows
    | "drop" ->
       rows := drop (arg_cols ()) !rows
    | "insert" ->
       let cte = arg_string () in
       let col = arg_int () in
       rows := insert (fun _ _ -> cte) col !rows
    | "random" ->
       let ub = arg_int () in
       let col = arg_int () in
       rows := insert (fun _ _ -> string_of_int (Random.int ub)) col !rows
    | "row" ->
       let col = arg_int () in
       rows := insert (fun i _ -> string_of_int i) col !rows
    | "row_parity" ->
       let col = arg_int () in
       rows := insert (fun i _ -> string_of_int (i mod 2)) col !rows
    | "minto" ->
       let cte = arg_int () in
       let col = arg_int () in
       let mincol = drop (other_cols [col] !rows) !rows in
       let mincol = aggregate_int min [0] mincol in
       let mincol = int_of_string (cell 0 0 mincol) in
       let shift s =
         let i = int_of_string s in
         string_of_int (i - mincol + cte)
       in
       rows := map_cols shift [col] !rows
    | "minto2" ->
       let cte = arg_int () in
       let cols = arg_cols () in
       let col = List.hd cols in
       let mincol = drop (other_cols [col] !rows) !rows in
       let mincol = aggregate_int min [0] mincol in
       let mincol = int_of_string (cell 0 0 mincol) in
       let shift s =
         let i = int_of_string s in
         string_of_int (i - mincol + cte)
       in
       rows := map_cols shift cols !rows
    | "eq" ->
       let cte = arg_string () in
       let col = arg_int () in
       let eq_cte v = if v = cte then "1" else "0" in
       rows := map_cols eq_cte [col] !rows
    | "not" ->
       let col = arg_int () in
       let onenot v = if v = "1" then "0" else "1" in
       rows := map_cols onenot [col] !rows
    | "1+300/x" ->
       let col = arg_int () in
       let f v =
         let x = float_of_string v in
         string_of_float (1.0 +. 300.0 /. x)
       in
       rows := map_cols f [col] !rows
    | "mult" ->
       let f = float_of_string (arg_string ()) in
       let cols = arg_cols () in
       rows := map_cols (fun s ->
                   let v = float_of_string s in
                   Printf.sprintf "%.2f" (f *. v)
                 ) cols !rows
    | "acctrunc" ->
       let cols = arg_cols () in
       let acc = ref 0. in
       rows := map_cols (fun s ->
                   let v = float_of_string s in
                   let i = int_of_float v in
                   acc := !acc +. v -. (float_of_int i);
                   let i =
                     if !acc >= 1.0 then (acc := !acc -. 1.0; i+1)
                     else i
                   in
                   Printf.sprintf "%d" i
                 ) cols !rows
    | "geq" ->
       let cte = float_of_string (arg_string ()) in
       let pred s =
         float_of_string s >= cte
       in
       let col = arg_int () in
       rows := filter pred col !rows
    | "leq" ->
       let cte = float_of_string (arg_string ()) in
       let pred s =
         float_of_string s <= cte
       in
       let col = arg_int () in
       rows := filter pred col !rows
    | "join_id" ->
       let j = arg_int () in
       let j' = arg_int () in
       let filename = arg_string () in
       let inp = open_in filename in
       let rows' = read_rows inp in
       close_in inp;
       (* let default j r =
         let nc' = ncols rows' in
         let fake = Printf.sprintf "%s?" r.(j) in
         Array.make nc' fake
       in *)
       rows := join_id !rows j j' rows'
    | "default_join_id_col" ->
       let j_dft = arg_int () in
       let j_id = arg_int () in
       let j_id' = arg_int () in
       let j_col' = arg_int () in
       let filename = arg_string () in
       let inp = open_in filename in
       let rows' = read_rows inp in
       close_in inp;
       let default _ r = Array.make 1 r.(j_dft) in
       let subarray r' = Array.make 1 r'.(j_col') in
       rows := join_id ~default ~subarray !rows j_id j_id' rows'
    | "percentile" ->
       let perc = arg_int_list () in
       let out = ref [] in
       let imax = List.length !rows - 1 in
       let compare s s' =
         let i s = try int_of_string s with _ -> 0 in compare (i s) (i s') in
       List.iter (fun p ->
           let rnk = rank compare (p * imax / 100) !rows in
           out := (string_of_int p :: row 0 rnk) :: !out
         ) perc;
       rows := List.rev !out
    | "average" ->
       let nrows = List.length !rows in
       Printf.eprintf "%d rows by %d cols\n" nrows (ncols !rows);
       let nrows = float_of_int nrows in
       let cols = other_cols [] !rows in
       let avg = aggregate_float_safe (+.) cols !rows in
       Printf.eprintf "avg";
       List.iter (fun s ->
           let f = (float_of_string s) /. nrows in
           Printf.eprintf "\t%.2f" f
         ) (row 0 avg);
       Printf.eprintf "\n";
       flush stderr
    | "stats" | "min_qmaq_max" ->
       (* qamd = 1st_quartile median average 3rd_quartile *) 
       if !rows = [] then invalid_arg "min_avg_max: no rows";
       let cols = other_cols [] !rows in
       let nrows = List.length !rows in
       Printf.printf "%d rows by %d cols\n" nrows (ncols !rows);
       flush stderr;
       let out = ref [] in
       let add name rows = out := (name :: row 0 rows) :: !out in
       aggregate_float_safe (min) cols !rows |> add "min";
       let compare s s' =
         let f s = try float_of_string s with _-> 0.0 in compare (f s) (f s') in
       rank compare (nrows / 4) !rows |> add "25%";
       rank compare (nrows / 2) !rows |> add "50%";
       let sum = aggregate_float_safe (+.) cols !rows in
       map_cols (let nrows = float_of_int nrows in
                 fun s ->
                 let f = (float_of_string s) /. nrows in
                 Printf.sprintf "%.2f" f
                ) cols sum |> add "avg";
       rank compare (3 * nrows / 4) !rows |> add "75%";
       aggregate_float_safe (max) cols !rows |> add "max";
       map_cols (fun s ->
           let f = float_of_string s in
           Printf.sprintf "%.2f" f
         ) cols sum |> add "sum";
       rows := List.rev !out
    | _ -> invalid_arg "unknown command, try help"    
  done;
  print_rows stdout !rows

  
let () =
  Printexc.record_backtrace true;
  try
    main ()
  with e ->
    flush stdout;
    Printf.eprintf "Error : %s\n%s\n" 
                   (Printexc.to_string e)
                   (Printexc.get_backtrace ());
    flush stderr

         
