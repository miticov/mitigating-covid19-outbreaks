# Author : Laurent Viennot, Inria 2020

import sys
import random
import math
import numpy as np


def unif_edges(n, m, contacts):
    g = {}
    for _ in range(m):
        u = random.randint(1,n)
        v = random.randint(1,n)
        u, v = (u, v) if u < v else (v, u)
        while u == v or (u,v) in g :
            u = random.randint(1,n)
            v = random.randint(1,n)
        g[(u,v)] = 1
    for _ in range(m, contacts):
        [e] = random.sample(g.keys(), 1)
        g[e] += 1
    return g

# same foot print but different graphs each day
def unif_edges_day(n, m, contacts, day, nb_days):
    g = {}
    for _ in range(m):
        u = random.randint(1,n)
        v = random.randint(1,n)
        u, v = (u, v) if u < v else (v, u)
        while u == v or (u,v) in g :
            u = random.randint(1,n)
            v = random.randint(1,n)
            u, v = (u, v) if u < v else (v, u)
        g[(u,v)] = 0
        # at least one contact one day:
        d = random.randint(0, nb_days-1)
        if d == day : g[(u,v)] = 1
    for _ in range(m, contacts):
        [e] = random.sample(g.keys(), 1)
        d = random.randint(0, nb_days-1)
        if d == day : g[e] += 1
    return g

def unif_contacts(n, contacts):
    g = {}
    for c in range(contacts):
        u = random.randint(1,n)
        v = random.randint(1,n)
        while u == v :
            u = random.randint(1,n)
            v = random.randint(1,n)
        u, v = (u, v) if u < v else (v, u)
        g[(u,v)] = g[(u,v)] + 1 if (u,v) in g else 1
    return g

def config_rank_size(n, contacts):
    # rank-size distribution of degrees :
    # deg(i) = deg_max * (1 - log(i)/log(n))
    # Sum_i deg(i) = deg_max * (n - log(n!)/log(n)) ~= deg_max * n / log(n)

    # generate degrees :
    deg_max = 2 * contacts * math.log(n) / n
    deg = np.zeros(n)
    for i in range(n) :
        d = deg_max * (1. - math.log(i) / math.log(n))
        deg[i] = max(1, round(d))
    deg_sum = np.zeros(n+1)
    for i in range(n):
        deg_sum[i+1] = deg_sum[i] + deg[i]
    if deg_sum[n] % 2 != 0:
        deg[0] += 1
        deg_sum[1] += 1
        
    # generate edges :
    half_edges = set()
    for i in range(n):
        for j in range(deg[i]) :
            half_edges.add(i)
    g = {}
    while len(half_edges) >= 2:
        u = random.sample(half_edges, 1)
        half_edges.remove(u)
        v = random.sample(half_edges, 1)
        half_edges.remove(v)
        if u != v:
            u, v = v, u if v < u else u, v
            g[(u,v)] = g[(u,v)] + 1 if (u,v) in g else 1
    return g

     

def print_graph(g, nb_groups=1):
    group_of={}
    def group(u):
        if not (u in group_of) :
            group_of[u] = random.randint(0, nb_groups-1)
        return group_of[u]
    for (u,v) in g:
        if g[(u,v)] > 0:
            if u < v : print(f'{u}\t{v}\t{g[(u,v)]}\t{group(u)}\t{group(v)}')
            else : print(f'{v}\t{u}\t{g[(u,v)]}\t{group(v)}\t{group(u)}')

            
seed = int(sys.argv[1])
n = int(sys.argv[2])
m = int(sys.argv[3])
contacts = int(sys.argv[4])
day = int(sys.argv[5])
nb_days = int(sys.argv[6])
nb_groups = int(sys.argv[7])

random.seed(seed)
g = unif_edges_day(n, m, contacts, day, nb_days)
print_graph(g, nb_groups)
