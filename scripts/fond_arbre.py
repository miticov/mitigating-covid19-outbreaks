# Author: Laurent Viennot, Inria 2020
import sys, math
import numpy as np
import matplotlib.pyplot as plt

from matplotlib.pyplot import figure

def get_data(input, day_col, exp_col, pre_col, len_col, fixed_last=0) :
    data = np.loadtxt(input, dtype=int, comments='idFrom', delimiter=' ', usecols=(day_col, exp_col, pre_col, len_col))
    # sort by column 0 :
    if data.shape == (4,) :
        data = data.reshape(1,4)
    data = data[data[:,0].argsort()]
    first = data[0,0]
    last = data[-1,0]
    if fixed_last > 0 : last = fixed_last
    return data, last - first + 1

scale_y = 4.
height_max = 5.629849271064987
prev_max = 8.

def plot_preval(n, data, dlen, fixed_last=0, y_max=0) :
    first = data[0,0]
    last = data[-1,0]
    if fixed_last > 0 : last = fixed_last
    print(f'days: {first} - {last} = {last - first}', file=sys.stderr)
    # prevalence, infected :
    prev = np.zeros((last + 1))
    inf = []
    infweek = np.zeros(int((last+7)/7), dtype=int)
    for [d, e, p, l] in data:
        inf.append(d)
        infweek[int(d/7)] += 1
        #print(d+e, p+l, file=sys.stderr)
        for i in range(p+l):
            j = d+e+i
            if j < len(prev) : prev[j] += 100./n
    ax = plt.subplot(111)
    plt.xlim(first - 9/math.sqrt(dlen), last+1) # - 1 -> - 9/math.sqrt(dlen)
    if y_max == 0 : y_max = max(scale_y, max(prev))
    plt.ylim(-0.1/n*100, y_max+0.1/n*100)
    # weeks :
    bins = range(0, last+14, 7)
    off = range(7, last+7, 14)
    wgt = np.ones_like(off) * y_max
    ax.hist(off, weights=wgt, bins=bins, color='gray', alpha=0.2)
    # infected :
    #print(inf*max(prev), file=sys.stderr)
    wgtinf=np.ones_like(inf)*y_max/2 / max(infweek)
    # bars of infected:
    # ax.hist(inf, weights=wgtinf, bins=bins, color='#00aa00', alpha=0.3)
    # steps of tot infeced:
    # totinf = np.zeros((last+1), dtype=float)
    # for [d, e, p, l] in data: totinf[d] += 1./float(len(data))*max(prev)
    # for i in range(1,len(totinf)): totinf[i] += totinf[i-1]
    # ax.step(range(1,last+2), totinf, color='#550055', alpha=0.5, linewidth=1.5)
    #
    # draw prevalence :
    ax.step(range(1,last+2), prev, color='#550055', alpha=0.5, linewidth=1.5)
    # first and last day :
    #plt.axvline(x=first)
    #plt.axvline(x=last)
    # axis :
    #ax.spines['left'].set_visible(False)
    ax.spines['right'].set_visible(False)
    ax.spines['top'].set_visible(False)
    ax.spines['bottom'].set_visible(False)
    #ax.set_xlabel('time')
    ax.set_ylabel('prevalence (%)')
    ax.tick_params(axis='x', which='both', bottom=False, top=False, labelbottom=False)
    #ax.tick_params(axis='y', which='both', left=False, right=False, labelleft=False)
    ax.axis('off')

def plot_axis(n, data, dlen, fixed_last=0, y_max=0) :
    first = data[0,0]
    last = data[-1,0]
    if fixed_last > 0 : last = fixed_last
    print(f'days: {first} - {last} = {last - first}', file=sys.stderr)
    # prevalence, infected :
    prev = np.zeros((last + 1))
    for [d, e, p, l] in data:
        for i in range(p+l):
            j = d+e+i
            if j < len(prev) : prev[j] += 100./n
    ax = plt.subplot(111)
    plt.xlim(first - 9/math.sqrt(dlen), last+1) # - 9/math.sqrt(dlen)
    if y_max == 0 : y_max = max(scale_y, max(prev))
    plt.ylim(-0.1/n*100, y_max+0.1/n*100)
    #ax.spines['left'].set_visible(False)
    ax.spines['right'].set_visible(False)
    ax.spines['top'].set_visible(False)
    ax.spines['bottom'].set_visible(False)
    #ax.set_xlabel('time')
    ax.set_ylabel('prevalence (%)')
    ax.tick_params(axis='x', which='both', bottom=False, top=False, labelbottom=False)

def plot_x_axis(n, data, dlen, fixed_last=0, y_max=0) :
    first = data[0,0]
    last = data[-1,0]
    if fixed_last > 0 : last = fixed_last
    print(f'days: {first} - {last} = {last - first}', file=sys.stderr)
    # prevalence, infected :
    prev = np.zeros((last + 1))
    for [d, e, p, l] in data:
        for i in range(p+l):
            j = d+e+i
            if j < len(prev) : prev[j] += 100./n
    ax = plt.subplot(111)
    plt.xlim(0 - 9/math.sqrt(dlen), last+1-first) # - 9/math.sqrt(dlen)
    if y_max == 0 : y_max = max(scale_y, max(prev))
    plt.ylim(-0.1/n*100, max(1.8, y_max+0.1/n*100))
    ax.spines['left'].set_visible(False)
    ax.spines['right'].set_visible(False)
    ax.spines['top'].set_visible(False)
    ax.spines['bottom'].set_visible(True)
    ax.set_xlabel('day')
    #ax.set_ylabel('prevalence (%)')
    ax.tick_params(axis='y', which='both', left=False, right=False, labelleft=False)
    #ax.tick_params(axis='x', which='both', bottom=False, top=False, labelbottom=False)


def max_prev(n, data, fixed_last=0) :
    first = data[0,0]
    last = data[-1,0]
    if fixed_last > 0 : last = fixed_last
    # prevalence, infected :
    prev = np.zeros((last + 1))
    for [d, e, p, l] in data:
        for i in range(p+l):
            j = d+e+i
            if j < len(prev) : prev[j] += 100./n
    return max(prev)


if __name__ == "__main__":
    fixed_last = 0
    if "daymax" in sys.argv : fixed_last = int(sys.argv[4])
    #
    data, dlen = get_data(sys.stdin, 3, 6, 7, 8, fixed_last)
    n = int(sys.argv[1])
    #
    width, height=16., 1.
    scale=488./8.
    reduce = scale * max(1., 1. + float(dlen) / 20.)
    if len(sys.argv) > 2 :
        w = float(sys.argv[2])
        h = float(sys.argv[3])
        hmin = 1.3 * 16 / 20
        height = max(hmin, width * h / w)
        #
        #w = (float(sys.argv[2]) - 0.) / reduce  * 1.09
        #h = (float(sys.argv[3]) * (0.92 - 0.00015*dlen)) / reduce * 1.09
        #height = width / (w / h + 0. / dlen)
        #
        #width = ((float(sys.argv[2]) - 10) / scale  - 1) / reduce * 1.09
        #height = (float(sys.argv[3]) / scale - 1) / 5 * 1.09
    print(f'size: {width} x {height}, ratio = {width/height}', file=sys.stderr)

    if "axis" in sys.argv:
        if "x" in sys.argv :
            figure(num=None, figsize=(width, 0.01))
            plot_x_axis(n, data, dlen, fixed_last, prev_max * height / height_max)
        else :
            figure(num=None, figsize=(0.01, height))
            plot_axis(n, data, dlen, fixed_last, prev_max * height / height_max)
        plt.savefig(sys.stdout.buffer, format='pdf', bbox_inches='tight')
        plt.close()
    else:
        figure(num=None, figsize=(width, height))
    
        plot_preval(n, data, dlen, fixed_last, prev_max * height / height_max)
        plt.savefig(sys.stdout.buffer, format='pdf', bbox_inches='tight')
        plt.close()

    # a * 1165 + b = 1485
    # a * 239 + b = 292
    # a = 1.2883, b = -10
