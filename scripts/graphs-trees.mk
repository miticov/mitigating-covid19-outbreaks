# Author: Laurent Viennot, Inria, 2020
# Makefile for : extracting graphs from SocioPatterns data, drawing graphs,
# and drawing trees from chains of transmission produced by the simulations.

#  ------------------- Scripts ------------------

# Manipulating files containing tables
OCAMLROWS:=ocaml scripts/rows.ml

# Draw cumulative distribution
PYTHONCUMDISTR:=python3 scripts/cumdistr.py


# --------- Download of SocioPatterns data ------------

DATASETS:=highschool primaryschool workplace

_data/workplace.csv: _data/workplace.dat_
	$(eval META:=_data/metadata_workplace.txt)
	cat $< | tr " " "\011" | $(OCAMLROWS) join_id 1 0 $(META) join_id 2 0 $(META) > $@

_data/%.csv: _data/%.dat_
	cat $< | tr " " "\011" > $@

_data:
	mkdir -p $@
	# Workplace 2
	curl -o $@/workplace.dat_.gz http://www.sociopatterns.org/wp-content/uploads/2018/12/tij_InVS15.dat_.gz
	curl -o $@/metadata_workplace.txt http://www.sociopatterns.org/wp-content/uploads/2018/12/metadata_InVS15.txt
	# Primary school
	curl -o $@/primaryschool.csv.gz http://www.sociopatterns.org/wp-content/uploads/2015/09/primaryschool.csv.gz
	curl -o $@/metadata_primaryschool.txt http://www.sociopatterns.org/wp-content/uploads/2015/09/metadata_primaryschool.txt
	# High school 3 (classes prepas)
	curl -o $@/highschool.dat_.gz http://www.sociopatterns.org/wp-content/uploads/2015/07/High-School_data_2013.csv.gz
	curl -o $@/metadata_highschool.txt http://www.sociopatterns.org/wp-content/uploads/2015/09/metadata_2013.txt
	cd $@; gunzip *.gz
	for d in $(DATASETS); do \
		echo $$d; \
		make _data/$$d.csv; \
	done;


# ----------- Trace visualization ----------------


# Plot column $(COL) by increasing values (almost cumulative distribution of the column) :
# Options in $(OPTS) :
# xlog : log scale for x
# ylog : log scale for y
# decr : sort values in decreasing order
%.cumdistr.pdf: %
	cat $< | $(PYTHONCUMDISTR) $(COL) $* $(OPTS) > $@

# Extract an interval of a trace from SEC_BEG to SEC_END - 1:
%.intervsec: %.csv
	cat $< | $(OCAMLROWS) format i,s,s,s,s \
		geq $$(( $(SEC_BEG) )) 0 \
		leq $$(( $(SEC_END) - 1 )) 0

# Extract a day from DAY days after SEC_OFFSET
_data/%.csv:
	$(eval DIR:=$(shell dirname $@)) 
	$(eval DAY:=$(shell basename $* | sed -E 's/^0*([1-9])/\1/'))
	@echo ----- Extracting $(DIR) $(DAY)
	mkdir -p $(DIR)
	$(eval BEG:=$(shell echo $$(($(DAY)*3600*24 + $(if $(SEC_OFFSET),$(SEC_OFFSET),0) ))))
	make -s --no-print-directory $(DIR).intervsec SEC_BEG=$(BEG) SEC_END=$$(( $(BEG)+3600*24 )) \
		> $@

# ----------- Graph extraction -------------------------

.PHONY: graphs
graphs: workplace primaryschool highschool random_unif

# format : u v nb_contacts u_group v_group
graphs/%.graph:
	make _data/$*.csv SEC_OFFSET=$(SEC_OFFSET)
	make $@.mkdir
	cat _data/$*.csv | $(OCAMLROWS) format s,s,s,s,s drop 0 insert 1 2 sum_int 2 > $@

workplace: 
	for day in 00 01 02 03 04 05 06 07 08 09 10 11 12 13; do \
		make graphs/$@/$$day.graph SEC_OFFSET=0; \
	done;
	cat graphs/$@/*.graph | $(OCAMLROWS) sum_int 2 > graphs/$@.graph

primaryschool: 
	for day in 00 01; do \
		make graphs/$@/$$day.graph SEC_OFFSET=0; \
	done;
	cat graphs/$@/*.graph | $(OCAMLROWS) sum_int 2 > graphs/$@.graph

# Trace starts at 12:00 (see README.md), hence the choice of 12*3600 bellow.
highschool:
	$(eval START:=$(shell head _data/$@.csv | $(OCAMLROWS) format i,s,s,s,s cell 0 0))
	$(eval OFFSET:=$(shell echo $$(( $(START) - 12*3600 )) ))
	@echo --- $(START) $(OFFSET)
	for day in 00 01 02 03 04; do \
		make graphs/$@/$$day.graph SEC_OFFSET=$(OFFSET); \
	done;
	cat graphs/$@/*.graph | $(OCAMLROWS) sum_int 2 > graphs/$@.graph

# Parameters obtained by : make graphs/highschool.deg.stats.cat | awk '{print($1,$3/2., $4/5./2.);}'
random_unif:
	mkdir -p graphs/$@
	python3 scripts/rand_graph.py 123 327 5818 37702 0 1 9 > graphs/$@/00.graph
	cat graphs/$@/*.graph | $(OCAMLROWS) sum_int 2 > graphs/$@.graph

#  ---- Graph stats ----

%.revgraph: %.graph
	cat $< | $(OCAMLROWS) format s,s,f,s,s swap 0 1 swap 3 4 > $@

%.deg: %.graph %.revgraph
	cat $^ | $(OCAMLROWS) format s,s,f,s,s drop 1,4,5 insert 1 1 sum_float 1,2 | sort -n -k 3 > $@

%.stats: %
	cat $< | $(OCAMLROWS) stats

%.perc: %
	cat $< | $(OCAMLROWS) percentile 0,1,5,10,15,20,25,30,40,50,60,70,75,80,85,90,95,99,100



#  ---- Graph drawing ----

# Draw graph
GRAPHVIZ:=neato -Ksfdp -Gbgcolor="\#00000000" -Goverlap=prism1 -Goverlap_scaling=3 -Gsplines=curved -Gmargin=0 -Nlabel="" -Nshape=circle -Nstyle=filled -Nwidth=.1 -Nfixed-size=true -Nfontsize=15 -Ncolor="\#000000FF" -Ecolor="\#00000020" -Earrowsize=3
DIGRAPHVIZ:=dot -Gbgcolor="\#00000000" -Grankdir=LR -Goverlap=scale -Gsplines=line -Nlabel="" -Nshape=circle -Nstyle=filled -Nwidth=.0001 -Nfixed-size=true -Nfontsize=15 -Ncolor="\#000000FF" -Ecolor="\#00000020" 
POSGRAPHVIZ:=neato -s -Gmargin=0 -Gbgcolor="\#00000000" -Gsplines=curved -Nlabel="" -Nshape=circle -Nstyle=filled -Nmargin=0 -Nfontsize=0 -Ncolor="\#000000FF" -Ecolor="\#00000020" 

draw_all_graphs:
	for g in graphs/*.graph; do \
		make  `echo $$g | sed -e 's/.graph//'`.deg; \
		make  `echo $$g | sed -e 's/.graph//'`.dotpos; \
	done
	for g in graphs/*.graph graphs/*/*.graph; do \
		make  `echo $$g | sed -e 's/.graph//'`.dot.pdf; \
	done
	make graphs/random_unif.dot.pdf

simple_pdf_names:
	for g in graphs/*.dot.pdf graphs/*/*.dot.pdf; do \
		mv $$g `echo $$g | sed -e 's/.dot.pdf$$/.pdf/'`; \
	done

%.dotdot: %.dot2
	$(GRAPHVIZ) -o $@ -Tdot $<

%.dot2: %.graph
	rm -f $*.dot
	make $*.dot THRESHOLD=1.1
	mv $*.dot $*.dot2

%.dotpos: %.dotdot
	cat $< | tr "\011\012" "àé" | sed -e 's/,é[à ]*/, /g' | tr "àé" " \012" | grep -E '^ *[0-9]+ *[[]' | sed -E 's/ *([0-9]+).*pos="([^"]*)".*/\1 \2/' > $@

%.dot.pdf: %.dot
	$(POSGRAPHVIZ) -o $@ -Tpdf $<

%.dot2.pdf: %.dot2
	$(GRAPHVIZ) -o $@ -Tpdf $<

%.png: %.pdf
	convert -density 100 -fill white -opaque none $< $@


GROUPCOLORS:=groupcol["1A"]="0.0 1.0 0.85"; groupcol["1B"]="0.05 0.5 1.0"; \
groupcol["2A"]="0.2 1.0 0.85"; groupcol["2B"]="0.25 0.5 1.0"; \
groupcol["3A"]="0.4 1.0 0.85"; groupcol["3B"]="0.45 0.5 1.0"; \
groupcol["4A"]="0.6 1.0 0.85"; groupcol["4B"]="0.65 0.5 1.0"; \
groupcol["5A"]="0.8 1.0 0.85"; groupcol["5B"]="0.85 0.5 1.0"; \
group["1753"]="1A"; \
group["1852"]="2A"; \
group["1746"]="3A"; \
group["1653"]="4A"; \
group["1668"]="5A"; \
group["1745"]="1B"; \
group["1650"]="2B"; \
group["1709"]="3B"; \
group["1521"]="4B"; \
group["1824"]="5B"; \
\
groupcol["2BIO1"]="0.0 1.0 1.0"; \
groupcol["2BIO2"]="0.11 1.0 1.0"; \
groupcol["2BIO3"]="0.22 1.0 1.0"; \
groupcol["MP"]="0.33 1.0 1.0"; \
groupcol["MP*1"]="0.44 1.0 1.0"; \
groupcol["MP*2"]="0.55 1.0 1.0"; \
groupcol["PC"]="0.65 1.0 1.0"; \
groupcol["PC*"]="0.77 1.0 1.0"; \
groupcol["PSI*"]="0.88 1.0 1.0"; \
\
groupcol["0"]="0.0 1.0 1.0"; \
groupcol["1"]="0.11 1.0 1.0"; \
groupcol["2"]="0.22 1.0 1.0"; \
groupcol["3"]="0.33 1.0 1.0"; \
groupcol["4"]="0.44 1.0 1.0"; \
groupcol["5"]="0.55 1.0 1.0"; \
groupcol["6"]="0.65 1.0 1.0"; \
groupcol["7"]="0.77 1.0 1.0"; \
groupcol["8"]="0.88 1.0 1.0"; \
\
groupcol["DCAR"]="0.0 1.0 0.8"; \
groupcol["DG"]="0.083 1.0 1.0"; \
groupcol["DISQ"]="0.166 1.0 1.0"; \
groupcol["DMCT"]="0.249 1.0 1.0"; \
groupcol["DMI"]="0.332 1.0 0.8"; \
groupcol["DSE"]="0.415 1.0 0.6"; \
groupcol["DST"]="0.498 1.0 1.0"; \
groupcol["SCOM"]="0.581 1.0 1.0"; \
groupcol["SDOC"]="0.644 1.0 1.0"; \
groupcol["SFLE"]="0.727 1.0 1.0"; \
groupcol["SRH"]="0.81 1.0 1.0"; \
groupcol["SSI"]="0.893 1.0 1.0";


%.dot: %.graph
	$(eval PLACE:=$(shell echo "$*" | sed -E 's|^graphs/([a-z]+).*|\1|'))
	$(eval DAYS:=1)
	$(eval G:=$(shell echo "$*" | sed -e 's|^graphs/||' -e 's|.graph$$||'))
	$(if $(filter primaryschool,$(G)),$(eval DAYS:=2),)
	$(if $(filter highschool,$(G)),$(eval DAYS:=5),)
	$(if $(filter workplace,$(G)),$(eval DAYS:=10),)
	$(eval THRESHOLD:=$(if $(CONTACT_THRESHOLD),$(CONTACT_THRESHOLD),0))
	@echo --- $(PLACE) $(DAYS) days $(THRESHOLD)
	(echo "digraph g {"; \
		make -s  --no-print-directory $*.deg.cat | cat $< graphs/$(PLACE).dotpos - graphs/$(PLACE).deg | awk '\
BEGIN{ $(GROUPCOLORS) } \
{ \
  if (NF == 2) { pos[$$1]=$$2; } \
  else if (NF == 5) { pres[$$1]=1; pres[$$2]=1; } \
  else if ( ! done[$$1]) { \
    done[$$1] = 1;\
    deg=$$3; lab=$$4; col="\#00000060"; \
    if ( ! pres[$$1]) deg=0; \
    if (lab == "Teachers") { lab="T" group[$$1]; col=groupcol[group[$$1]] } \
    if (groupcol[lab] != "") { col=groupcol[lab]; } \
    posnode = ""; \
    if (pos[$$1]) { posnode = "pos=\"" pos[$$1] "!\""; } \
    printf("  %s [ fillcolor=\"%s\" width=\"%f\" penwidth=\"1.0\" %s ];\n", \
           $$1, col, (1.5 + deg/1000/$(DAYS))*0.1, posnode); \
  } \
}'; \
		cat $< | awk '{ if($$3 >= $(THRESHOLD) * $(DAYS)) { \
  col="#00000060"; \
  if ($$3 <= 4  * $(DAYS)) col="#00000040"; \
  if ($$3 <= 2 * $(DAYS)) col="#00000020"; \
  if ($$3 < 1 * $(DAYS)) col="#00000010"; \
  print("  ", $$1, " -> ", $$2, "  [ penwidth=\"" (0.25 + $$3/20.0/$(DAYS))*0.4 \
        "\" weight=\"" ($$3 / $(DAYS)) "\" dir=none color=\"" col "\"];"); } }'; \
	echo "}") > $@


EXTDIR:=simulations/_graph

ext_graphs:
	@if [ ! -d $(EXTDIR) ]; then \
		echo "No $(EXTDIR) dir, type 'make simulations' first."; exit 1; \
	fi
	$(eval P:=highschool)
	make  graphs/$(P).deg
	make  graphs/$(P).dotpos
	for e in complete scaling sorting; do \
		cat $(EXTDIR)/$(P).ext-type=$$e.txt | tr " " "\011" > graphs/$(P)-$$e.graph; \
		make graphs/$(P)-$$e.dot.pdf; \
	done

# ------------------- drawing trees --------------

TRACEDIR:=simulations/_trace

draw_median_trees:
	@if [ ! -d $(TRACEDIR) ]; then \
		echo "No $(TRACEDIR) dir, type 'make simulations' first."; exit 1; \
	fi
	for p in `/bin/ls $(TRACEDIR)`; do \
		for r in `head -n 2 $(TRACEDIR)/$$p/description.txt | tail -n 1 | tr -d ",[]" | awk '{print($$1,$$2,$$3,$$4);}'`; do \
			echo $$r; \
			mkdir -p _trees/$$p; \
			make _trees/$$p/$$r.pdf PLACE=$$p RUN=$$r; \
		done; \
	done



_trees/%.pdf:
	$(eval TMP:=$(shell mktemp _latex_XX))
	$(eval DIR:=$(TMP)_dir)
	mkdir -p $(DIR)
	for s in none onoff1d onoff1w rotation1d rotation1w closed; do \
		make $(TRACEDIR)/$(PLACE)/$$s/$(RUN)-tree.pdf; \
		make $(TRACEDIR)/$(PLACE)/$$s/$(RUN)-back.pdf STRAT=$$s; \
		make $(TRACEDIR)/$(PLACE)/$$s/$(RUN)-y-back.pdf STRAT=$$s ARGS=axis; \
		strat='No strategy'; \
		if [ $$s = "onoff1d" ]; then strat='On/Off 1 day in 2'; fi; \
		if [ $$s = "onoff1w" ]; then strat='On/Off 1 week in 2'; fi; \
		if [ $$s = "rotation1d" ]; then strat='Rotation 1 day in 2'; fi; \
		if [ $$s = "rotation1w" ]; then strat='Rotation 1 week in 2'; fi; \
		if [ $$s = "closed" ]; then strat='Full telecommuting'; fi; \
		echo "\\\\tabline{../$(TRACEDIR)/$(PLACE)/$$s/$(RUN)-y-back.pdf}{../$(TRACEDIR)/$(PLACE)/$$s/$(RUN)-back.pdf}{../$(TRACEDIR)/$(PLACE)/$$s/$(RUN)-tree.pdf}{$$strat}" >> $(DIR)/trees.tex; \
	done
	make $(TRACEDIR)/$(PLACE)/none/$(RUN)-x-back.pdf STRAT=none ARGS="x axis"
	cd $(DIR); \
	ln -s ../scripts/stack.tex .; \
	ln -s ../$(TRACEDIR)/$(PLACE)/none/$(RUN)-x-back.pdf x-axis.pdf; \
	pdflatex stack
	mv $(DIR)/stack.pdf $@
	rm -fr $(DIR)
	rm -f $(TMP)


%-tree.pdf: %-tree.dot
	$(POSGRAPHVIZ) -o $@ -Tpdf $<

%-back.pdf:
	$(eval TR:=$(TRACEDIR)/$(PLACE)/$(STRAT)/$(RUN))
	$(eval DAYMAX:=$(shell cat $(TRACEDIR)/$(PLACE)/*/$(RUN).txt | grep -v -e 'idFrom idTo symptFrom' | awk '{print($$4)}' | sort -n | tail -n 1))
	make -s --no-print-directory $(TR).sympt1.cat | python3 scripts/fond_arbre.py \
  `make -s --no-print-directory graphs/$(PLACE).deg.lines` \
  `identify $(TR)-tree.pdf | sed -E 's/.* PDF ([0-9]*)x([0-9]*).*/\1 \2/'` \
  $(DAYMAX)  daymax $(ARGS) > $@


%-treepos-dot.dot: %-treepos.dot
	$(DIGRAPHVIZ) -o $@ -Tdot $<

%-tree.posxy: %-treepos-dot.dot
	cat $< | tr "\011\012" "àé" | sed -e 's/,é[à ]*/, /g' | tr "àé" " \012" | grep -E '^ *[0-9]+ *[[]' | sed -E 's/ *([0-9]+).*pos="([^"]*)".*/\1 \2/' | tr "," " " > $@

%.srttxt: %.txt
	sort -n -k 4 $< | grep -v -e 'idFrom idTo symptFrom'  > $@

# correct lenSympt to 1 for Symptomatics 
%.sympt1: %.txt
	cat $< | grep -v -e 'idFrom idTo symptFrom' | awk '{len=$$9; if($$6 == "S") len=1; print($$1,$$2,$$3,$$4,$$5,$$6,$$7,$$8,len);}' > $@

%-tree.dot: %.srttxt
	$(eval PLACE:=$(shell echo "$*"  | sed -e 's|$(TRACEDIR)/||' | sed -e 's|/.*||'))
	$(eval NAME:=$(shell basename $*))
	$(eval DEGFACTOR:=0.2)
	$(if $(filter highschool,$(PLACE)),$(eval DEGFACTOR:=0.15),)
	$(if $(filter workplace,$(PLACE)),$(eval DEGFACTOR:=0.15),)
	$(eval G:=graphs/$(PLACE))
	$(eval R:=$(shell cat $< | grep -e -1 | awk '{print($$2)}'))
	$(eval PMIN:=$(shell cat $(TRACEDIR)/$(PLACE)/*/$(NAME).txt | grep -v -e 'idFrom idTo symptFrom' | grep -v -e -1 | awk '{print($$5*1000)}' | sort -n | head -n 1)/1000.)
	$(eval PMAX:=$(shell cat $(TRACEDIR)/$(PLACE)/*/$(NAME).txt | grep -v -e 'idFrom idTo symptFrom' | grep -v -e -1 | awk '{print($$5*1000)}' | sort -n | tail -n 1)/1000.)
	$(eval DAYMIN:=$(shell cat $(TRACEDIR)/$(PLACE)/*/$(NAME).txt | grep -v -e 'idFrom idTo symptFrom' | awk '{print($$4)}' | sort -n | head -n 1))
	$(eval DAYMAX:=$(shell cat $(TRACEDIR)/$(PLACE)/*/$(NAME).txt | grep -v -e 'idFrom idTo symptFrom' | awk '{print($$4)}' | sort -n | tail -n 1))
	echo " -------- $(PMIN) $(PMAX) $(DAYMIN) $(DAYMAX) ---------"
	$(eval P:=$*-tree.posxy)
	make $(P)
	(echo "digraph g {"; \
	make -s --no-print-directory $(G).deg.cat | awk '{print($$1,$$3,$$4,"dum")}' | cat $(P) $< - | awk 'BEGIN{ \
  $(GROUPCOLORS) \
  dur = $(DAYMAX) - $(DAYMIN); \
  durfactor = 1.0; \
  if (dur < 4*7) durfactor = 0.5; \
  if (dur >= 4*7 && dur < 10 * 7) durfactor = 0.5 + 0.5*(dur-4*7)/6/7 ; \
  dum=10000; \
  printf("%d [ fillcolor=\"#00000000\" color=\"#00000000\" width=\"1.068340\" penwidth=\"5.0\" pos=\"%f,0!\"];\n", dum, 0.5 + dur * 37); \
  printf("%d [ fillcolor=\"#00000000\" color=\"#00000000\" width=\"1.068340\" penwidth=\"5.0\" pos=\"%f,55!\"];\n", dum+1, 0.5 + dur * 37); \
} {  \
  if (NF == 3) { \
    posx[$$1]=$$2; \
    posy[$$1]=$$3; \
  } else if (NF > 4) { \
    pres[$$1]=1; pres[$$2]=1; \
    sympt[$$2] = $$6 == "S"; \
  } else { \
    deg=$$2; lab=$$3; col="\#00000060"; \
    if (lab == "Teachers") { lab="T" group[$$1]; col=groupcol[group[$$1]] } \
    if (groupcol[lab] != "") { col=groupcol[lab]; } \
    pencol="red"; \
    if (sympt[$$1]) pencol="\#0000AAFF"; \
    if (pres[$$1]) printf("  %s [ fillcolor=\"%s\" color=\"%s\" width=\"%f\" penwidth=\"%f\" pos=\"%f,%f!\"];\n", $$1, col, pencol, (0.5 + deg * $(DEGFACTOR)/1000) * durfactor, 5.0 * durfactor, posx[$$1], posy[$$1] * durfactor); \
  } \
}'; \
	cat $< | awk '\
BEGIN{ \
  $(GROUPCOLORS) \
  dur = $(DAYMAX) - $(DAYMIN); \
  durfactor = 1.0; \
  if (dur < 4*7) durfactor = 0.5; \
  if (dur >= 4*7 && dur < 10 * 7) durfactor = 0.5 + 0.5*(dur-4*7)/6/7 ; \
} { c = "0.11 1.0 1.0"; \
  if ($$3 == "A") c = "red"; \
  if ($$3 == "S") c = "blue"; \
  if ($(PMAX) == $(PMIN)) p = 3.0; \
  else p = 3.0 + ($$5 - $(PMIN)) / ($(PMAX) - $(PMIN)) * 7.; \
  arr=3./(p-2.5)**0.5; \
  if (arr > 15.) arr=15.; \
  if ($$1 != "-1") print("  ", $$1, " -> ", $$2, "  [ color=\"" c "\" penwidth=\"" (p * durfactor) "\" arrowsize=\"" (arr * durfactor) "\" ];"); \
}'; \
	echo "}") > $@
	rm -f $(P)

# add fake edges so that rank is day of infection
%-treepos.dot: %.srttxt
	$(eval PLACE:=$(shell echo "$*"  | sed -e 's|$(TRACEDIR)/||' | sed -e 's|/.*||'))
	$(eval DEGFACTOR:=0.5)
	$(if $(filter highschool,$(PLACE)),$(eval DEGFACTOR:=0.2),)
	$(if $(filter workplace,$(PLACE)),$(eval DEGFACTOR:=0.1),)
	$(eval G:=graphs/$(PLACE))
	$(eval R:=$(shell cat $< | grep -e -1 | awk '{print($$2)}'))
	(echo "digraph g {"; \
	make -s --no-print-directory $(G).deg.cat | awk '{print($$1,$$3,$$4)}' | cat $< - | awk '{ if (NF > 3) {pres[$$1]=1; pres[$$2]=1; } else { deg=$$2; lab=$$3; col="\#00000060"; if (lab == "Teachers") { lab="T"; col="red"; } if ($$1 == "$(R)") col="blue"; wdt=0.1 + 1.0*deg*$(DEGFACTOR)/1000; if (pres[$$1]) printf("  %s [ color=\"%s\" width=\"%f\"];\n", $$1, col, .0001); } }'; \
	cat $< | awk '{print($$2,$$4)}' | cat - $< | awk 'BEGIN{ dum=10000; } { if (NF == 2) { if(! seen[$$1] || $$2 < jourinf[$$1]) {seen[$$1]=1; jourinf[$$1]=$$2;} } else if ($$1 != "-1") { c = "green"; if ($$3 == "A") c = "red"; if ($$3 == "S") c = "blue"; p = 0.1 + 5.0 * $$5 / 0.0025; len = jourinf[$$2] - jourinf[$$1]; print("  ", $$1, " -> ", $$2, "  [ color=\"" c "\" penwidth=\"" p "\" ];"); for(i=1; i<len; i++){ if(i==1) print($$1, " -> ", dum++, ";"); if(i == len - 1) print(dum - 1, " -> ", $$2, ";"); else print(dum - 1, " -> ", dum++, ";"); } } }'; \
	echo "}") > $@

# --------------------- misc ---------------------

# $(call dasharg,i) cut $* according to '-' and returns the ith substring :
dasharg=$(word $(1),$(subst -, ,$*))

test:
	@echo $(if $(A),yes$(A),no) $(basename a/c/b.txt)

clean:
	rm -f *~
	rm -f graphs/*.dot* graphs/*/*.dot* graphs/*.deg graphs/highschool-*
	find $(TRACEDIR) -name '*-*' -exec rm -f {} \;
	rm -fr _latex*

purge: clean
	rm -fr _*
	cd simulations; make clean

%.op:
	make $*
	open $*

%.cat: %
	@cat $<

%.head: %
	@head $<

%.tail: %
	@tail $<

%.wc: %
	@wc $<

%.lines: %
	@wc -l $< | awk '{print($$1);}'

%.force:
	@rm -f $*; make $*

%.forceop:
	@rm -f $*; make $*; open $*

%.rm:
	rm -i $*

%.mkdir:
	mkdir -p $(dir $*)

%.md:
	make $*
	@echo
	@echo '```'
	@echo "make $*"
	@echo '```'
	@echo
	@b=`basename $*`; if [ "$$b" != "`basename $* .pdf`" ] ; then \
		f=`echo $* | tr "/" "_"`; \
		cp -f $* figs/$$f; \
		echo "![Figure $$b](figs/$$f)"; \
		echo; \
	fi

