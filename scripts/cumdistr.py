# Author : Laurent Viennot, Inria 2020

import sys
import math
import numpy as np
import matplotlib.pyplot as plt


def plot_cum_distr(input, j_col, xlab, weighted=False) :
    data = np.loadtxt(input, dtype=float, comments='#', delimiter='\t', usecols=(j_col))
    #
    col = np.sort(data[:])
    #
    if decr: col[::-1].sort()
    #
    n = len(col)
    #
    #b = math.ceil(math.log(n) / math.log(2))
    nbins = 20
    bins = [col.min() + i * (col.max() - col.min()) / nbins for i in range(nbins+1)]
    if xlog:
        bins = [col.min() + math.exp(i * math.log(col.max() - col.min() + 1) / nbins) - 1 for i in range(nbins+1)]
    weights = np.ones_like(col) / len(col)
    if weighted:
        weights = col / col.sum()
    #
    #plt.plot(col, [float(i)/n for i in range(1,n+1)])
    fig, ax1 = plt.subplots()
    color = 'tab:blue'
    ax1.set_xlabel(f'n={n} sum={col.sum()} col={j_col} {xlab}')
    ax1.set_ylabel('probality distribution', color=color)
    ax1.hist(col, weights=weights, bins=bins, alpha=0.4)
    ax1.tick_params(axis='y', labelcolor=color)
    ax1.xaxis.grid(True)
    #
    #plt.hist(col, weights=weights, bins=bins, alpha=0.4)
    ax2 = ax1.twinx()  # instantiate a second axes that shares the same x-axis
    color = 'tab:red'
    ax2.set_ylabel('cumulative distribution', color=color)
    if not weighted:
        ax2.plot(col, [float(i)/n for i in range(1,n+1)], color=color)
    else:
        pref_sum = np.copy(col)
        for i in range(1,n):
            pref_sum[i] = pref_sum[i-1] + col[i]
        ax2.plot(col, [float(pref_sum[i])/pref_sum[n-1] for i in range(n)], color=color)
    ax2.tick_params(axis='y', labelcolor=color)
    ax2.grid(True)
    #
    if xlog: plt.xscale('log')
    #if ylog: plt.yscale('log')
    #plt.grid(True)
    #
    fig.tight_layout()  # otherwise the right y-label is slightly clipped


j_col = int(sys.argv[1])
xlab = sys.argv[2]

xlog = 'xlog' in sys.argv
ylog = 'ylog' in sys.argv
decr = 'decr' in sys.argv
wgth = 'wgth' in sys.argv

plot_cum_distr(sys.stdin, j_col, xlab, weighted=wgth)

plt.savefig(sys.stdout.buffer, format='pdf')
plt.close()

