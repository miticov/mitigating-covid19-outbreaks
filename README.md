# mitigating-covid19-outbreaks

This repository provides the necessary code to reproduce the simulations performed in [Analysis of mitigation of Covid-19 outbreaks in workplaces and schools by hybrid telecommuting](https://www.medrxiv.org/content/10.1101/2020.11.09.20228007v1) by Simon Mauras, Vincent Cohen-Addad, Guillaume Duboc, Max Dupré la Tour, Paolo Frasca, Claire Mathieu, Lulla Opatowski, and Laurent Viennot.

Relying on the graph data provided here, you can reproduce the simulations of the paper with:

 * `make simulations`

A C++ compiler and python3 with various modules (numpy, pandas, matplotlib, ...) are required.

To download the original data from [SocioPatterns](http://www.sociopatterns.org/datasets/), re-generate the graphs, perform simulations, and generate all figures of the paper:

 * Install ocaml (use [opam tool](https://ocaml.org/docs/install.fr.html)), [Graphviz](https://graphviz.org/download/) and [ImageMagick](https://imagemagick.org/script/download.php),
 * `make all`


