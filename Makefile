# Authors: Guillaume Duboc, Max Dupré la Tour, Simon Mauras and Laurent Viennot, 2020.

# Scrips for generating the graphs used in https://www.medrxiv.org/content/10.1101/2020.11.09.20228007v1

help:
	@echo "Read Readme.md, and then 'make simulations'."

simulations:
	cd simulations; make all

all: regenerate_graphs draw_all_graphs simulations ext_graphs draw_median_trees

regenerate_graphs: _data
	mv graphs provided_graphs
	make graphs
	diff graphs provided_graphs


repository: _data graphs purge

reinit: purge
	rm -fr graphs


include scripts/graphs-trees.mk
